#!/usr/bin/env ruby

require 'json';

$scenes = {};
$branches = [];
$ids = {};

def fail(*lines)
  puts lines;
  exit 1;
end

def assert_key(json, key, msg:"Key not found")
  if !json.key? key
    puts json;
    fail "#{msg}: #{key}";
  end
end

def assert_empty(thing, msg:"Expected to be empty")
  if !thing.empty?
    fail "#{msg}: #{thing}";
  end
end

def assert_file(path)
  if !File.exist? path
    fail "File not found: #{path}";
  end
end

def assert_src(src)
  if !src.is_a? Array;
    src = [src];
  end
  src.each{ |file|
    assert_file(file);
  };
end

def assert_support(keys,supported,unsupported=[])
  assert_empty(keys-supported-unsupported, msg:"Unknown key(s)") # unknown keys.
  assert_empty(keys-supported, msg:"Unsupported key(s)") # unsupported keys.
end

def assert_media(media)
  if media.is_a? Array
    media.each{ |media|
      assert_media(media);
    };
    return;
  end

  if !media.key? "type"
    # In some cases, it's acceptable to omit the "type" attribute,
    # if from the context it is clear which type the media has.
    # For example, instead of typing
    # {
    #		"type": "video",
    #		"src": "video_file.mp4"
    # }
    # We can simply type:
    # { "video": "video_file.mp4" }
    # Here are the mappings from those keys.
    types = [ "video", "audio", "image", "button", "delay" ];
    types += [ "loop", "queue", "random" ];
    types += [ "all", "sync" ];
    types += [ "goto" ];
    types += [ "stop", "play" ];
    types = (media.keys & types);
    if types.length != 1
      puts "#{media.keys}";
      fail "Failed to determine type of media: #{types}";
    else
      type = types.first;
      # Type was successfully determined
      media["type"] = type;
      # Src becomes the value of this type
      media["src"] = media[type];
      media.delete type;
    end
  end

  # All forms of media can have these keys.
  all_keys = ["type", "src", "id"];
  all_keys += ["fade", "next", "wait"];

  # Type specifics.
  assert_key(media, "type");

  case media["type"]
  when "video", "audio" # Dynamic media.
    # A video/audio should always have a src, or multiples.
    assert_key(media, "src");

    assert_src(media["src"]);

    assert_support(media.keys, all_keys+["volume","speed"], ["loop"]);
  when "image"
    assert_key(media, "src");

    assert_src(media["src"]);

    assert_support(media.keys, all_keys+["wait"]);
  when "queue" # Basic (relay) media, doesn't do much on its own.
    assert_support(media.keys, all_keys);
    assert_media(media["src"]);
  when "random"
    assert_support(media.keys, all_keys+["ratio"]);
    # If ratio is defined, assert that the length matches the src length.
    if media.key?"ratio"
      if media["ratio"].size != media["src"].size
        fail "Ratio size and media size do not match! (#{media["ratio"].size}!=#{media["src"].size})";
      end
    end
    assert_media(media["src"]);
  when "loop"
    assert_support(media.keys, all_keys+["wait","times"]);
    assert_media(media["src"]);
    assert_media(media["wait"]) if media.key? "wait";
  when "all"
    assert_support(media.keys, all_keys+["until"]);
    assert_media(media["src"]);
  when "multi"
    assert_support(media.keys, all_keys+["wait"]);
    assert_media(media["src"]);
  when "button"
    # All buttons should have text.
    assert_support(media.keys, all_keys+["text", "key", "anchor", "class"]);
  when "delay"
    assert_key(media, "src")
    assert_support(media.keys, all_keys);
  when "branch"
    # Any keys might be present.
    assert_key(media, "src")

    # Make sure "src" points to a valid key.
    assert_key(media, media["src"]);

    $branches << media;

    # Make sure that the branches are valid.
    media.each{ |k,v|
      next if k == 'src' || k == 'type';
      assert_media(v);
    };
  when "goto"
    assert_key(media, "src")

    assert_support(media.keys, all_keys+["force"]);

    # Make sure we can find the scene we are going to.
    if not $scenes.key? media['src']
      fail "Cannot find scene '#{media["src"]}' from scenes #{$scenes}!";
    end
  when "sync"
    assert_key(media, "src");

    assert_key(media, "with");
    assert_media(media["src"]);
    assert_media(media["with"]);
  when "stop", "play"
    # Src is the scene ID(s) that we're stopping/playing.
    assert_key(media, "src");

    # Make sure we can find the ID.
    # TODO: This needs to be done when we have registered all of our IDs.
    # assert_key($ids, media["src"]);
  else
    fail "Unknown media type: #{media["type"]}"
  end

  # If we have an ID, put it in ids if it doesn't exist yet.
  $ids[media["id"]] = media if media.key?("id") && !$ids.key?(media["id"]);
end

if ARGV.empty?
  fail "Usage: test [options.json]";
end

json = JSON.parse(File.read(ARGV.first));

Dir.chdir(File.dirname(ARGV.first));

# Check for unsupported/unknown keys at root.
assert_support(json.keys, ["config", "main", "scenes"], ["start", "media"]);

# Copy ids over.
if json.key?"scenes";
  $scenes = json["scenes"];
end

# Check for unsupported/unknown keys in config.
# Config doesn't need to exist.
json["config"]={} if !json.key? "config";
assert_support(json["config"].keys, ["loading", "cover", "title", "blur", "bg", "controls"]);

# Check for unsupported/unknown in config.loading.
json["config"]["loading"]={} if !json["config"].key? "loading";
if json["config"]["loading"].is_a? Hash
  assert_support(json["config"]["loading"].keys, ["text", "image", "bar"]);
end

# Check through scenes.
$scenes.each{ |_,media|
  assert_media(media);
};

# Check through main.
assert_media(json["main"]) if !json["main"].nil?;
