# CLIP - Lightweight Interactive Player

CLIP is a Lightweight Interactive **HTML5** & **JavaScript** -based Web Player, intended to act as replacement for Flash.
It supports multiple forms of media and simultaneous playback of audio/video in complex structures.
The player can be customised and configured using JSON rules (see below).

If you're an artist, please read [Artist Set-Up](#artist-set-up) for instructions how to set up CLIP.

For developers and contributors, please refer to [Developer Set-Up](#developer-set-up) below.

## Artist Set-Up

In order to start creating interactive experiences with CLIP, you will need to get the
latest build from this repository.

Start by downloading the latest release builds on your local machine; you can do this by clicking the
link below.
This will give you **all the builds, and you need to pick one version**.

There should be a few folders such as `release.json-build-v1.051-8ea774`.

The version number such as **v1.051** here is important, and you should generally pick the latest one, but feel free to pick and experiment with any of the versions.

[Download build/release](https://gitlab.com/ilmikko/clip-ci/-/archive/master/clip-ci-master.zip?path=build/release)

Once you have picked a folder, you can now edit the `index.html` inside of it.
You generally don't need to touch anything else than the JSON between the `Player.load({ ... })` statement, which can be found at the very bottom of the HTML.

For example, if your JSON was:
```
"config": {
	...
},
"scenes": {
	"main": { ... }
}
```

Then the HTML page would be:
```
<script>Player.load({

"config": {
	...
},
"scenes": {
	"main": { ... }
}

})</script>
```

Do NOT edit anything outside of the `<script>` tags unless you know what you're doing.
It's a great way to break the entire player, and you'd have to start over with the build.

### Formats

At the time of writing, CLIP supports the following formats:

Video: webm, mp4

Audio: ogg, mp3, wav

Image: webp, png, jpg

Some other formats are supported but not in all browsers.
See [browser support](https://en.wikipedia.org/wiki/HTML5_video#Browser_support) for more information.

Do note that video with embedded audio will be muted and not used in CLIP.
This is because of browser restrictions and the way video media is preloaded.
If you need audio in your videos, please use the `sync` element.

In case there are any queries, please send them directly to my email (ilmikko at 5x.fi).
I'll be happy to help and update the documentation.

### Basic structures

The format of JSON is as follows:

You can have objects that are denoted by curly brackets `{}`.
In CLIP an object always needs to have key value pairs inside of it.
These are denoted as `{ "key": "value" }`.

A value can be another object.

If a value is **multiple** objects, it needs to be denoted by square brackets `[]`.
The contents of the brackets needs to be separated by commas as `[ { ... }, { ... }, { ... } ]`.
This is the case for elements such as `all`, `queue` and `random`.

A value can also be text `"text"` or a number `42`.

With these rules you can build complex structures, such as:

```
"scenes": {
	"main": {
		"all": [
			{ "video": "./media/video.mp4" },
			{ "audio": "./media/audio.mp3" },
		]
	}
}
```

In this example, there is a video element that contains a text value (the video path).
There is also an audio element that has a similar text value.

These elements are contained within an `all` element - in square brackets, because the `all` element contains **multiple** objects.

The `all` element is contained in a `main` element - in curly brackets because there is only one element inside.

And finally, the `main` element is contained within `scenes`.

You should use a formatting tool when writing JSON. Trust me, it will make your life so much easier.
Here are a few good ones: [VSCode](https://code.visualstudio.com/), [Atom](https://atom.io/), [Vim](https://www.vim.org/)

You can find more information on the JSON format here: https://en.wikipedia.org/wiki/JSON#Example

#### Scenes

Scenes are collections of media that play at specific times when the player operates.

The player will always first play the `main` scene. If this is not defined, the player will return an error.

A simple structure could be:

```
"scenes": {
	"main": { "goto": "scene1" },
	"scene1": {
		"queue": [
			{ "video": "./media/scene1.mp4" },
			{ "goto": "scene2" }
		]
	},
	"scene2": { ... }
}
```

#### Loops

Sometimes you want to loop a scene until a precondition has been reached - for example, until user input is received.

You can use the `wait` property of the `loop` element along with buttons.

```
"scenes": {
	"main": {
		"loop": { "./media/loop.mp4" },
		"wait": {
			"all": [
				{ "button": "Scene 1", next: { "goto": "scene1" } },
				{ "button": "Scene 2", next: { "goto": "scene2" } }
			]
		}
	},
	"scene1": { ... },
	"scene2": { ... }
}
```

Because "all" will play all of the media elements, it will show all of the buttons while the loop is active.
However, only one of the buttons may be pressed as doing so will change the scene to a different one.

#### Synchronizing video and audio

Even if your audio clips are the same "length" as your video clips, they will fall out of sync eventually.
This is due to the technicalities between encoding of video and audio as there are no "frames" in an audio waveform as you would have in video.

Falling out of sync can be fixed by using the `sync` element.
Your audio files should be slightly longer than the video files, so when the video loops, the audio hasn't stopped yet.

```
"loop": {
	"sync": { "audio": "./media/aud1.mp3" },
	"with": { "video": "./media/vid1.mp4" }
}
```

Do note that the order matters here.
The element inside `sync` can be abruptly stopped and started based on the element inside `with`.
It's preferable to have audio stopped/started based on a video, rather than the other way around.

#### Background Audio

If you need background audio over multiple scenes, you will need to use the `play` element.
Otherwise the background audio would need to be restarted for each scene, as a scene change will stop all the media elements inside such scene.

```
"scenes": {
	"main": {
		"all": [
			{ "play": "background" },
			{ "goto": "scene1" },
		]
	},
	"background": {
		"loop": { "audio": "./media/background.mp3" }
	},
	"scene1": { ... }
}
```

#### Seamless Audio

For long looping audio such as background audio, it can be beneficial to use the `fade` property to perform a **crossfade** between loops of the audio.

Having two audio elements playing back-to-back when crossfading effectively hides the seam, but may have to be fine-tuned on a case-by-case basis.

```
"loop": {
	"queue": [
		{ "audio": "./media/background.mp3", "fade": "25%" },
		{ "audio": "./media/background.mp3", "fade": "25%" }
	]
}
```

Having a `fade` property of `"25%"` (mind the quotes) means that the audio will finish fading in at 25% of its length, and start fading out when the last 25% of the length is left.
See the [Fade](#fade-property) section for more information.

## JSON documentation

The player supports several options that you can pass formatted as JSON.
These options will be the 'barebones' of the animation, and will contain
information about the functionality of the interactive video you want to show.

### Skeleton example

Every JSON needs at least this information to work.
There are two keys that are separate from each other:

The `config` key contains information how the player is configured for a session.

The `scenes` key contains all the scenes that will be played, in a tree-like structure.
This includes a "main" scene which is always played first.

```
{
	"config": {
		...
	},
	"scenes": {
		...
	}
}
```

### Options for the `config` section

The config section contains specific bits of configuration necessary to
customize how the player functions or looks for a specific session.

Below you can find a list of the options in more detail.

#### `config.element` - Player Element

The element property contains a single element or an identifier of an element,
that will act as the player element. Usually this does not need to be specified,
as the player element is considered to be the element with ID `player`.
However, in some cases alternate elements are preferred, in which case you can
change this property.

Example usage:
```
"element": document.getElementById("player-id")
```
or
```
"element": "#player-id"
```

#### `config.controls` - Player Controls

The controls section contains information which controls are displayed in the
player at all times. For example, you can remove the play/pause button if it is
not needed.

The default value for this section is:
```
"controls": [
	{ "src": "control-pause", "location": "sw" },
	{ "src": "control-volume", "location": "sw", "min": 0, "max": 1, "default": 0.5 },
	{ "src": "control-speed", "location": "sw", "min": 0.5, "max": 3, "default": 1 },
	{ "src": "control-hide", "location": "se" },
	{ "src": "control-fullscreen", "location": "se" }
]
```

You can also use a shorthand version to just enable/disable default controls.

For example, to only enable pause/hide/fullscreen (a.k.a. disable volume and speed controls):
```
"controls": [ "pause", "hide", "fullscreen" ]
```

Properties:

`src` - The source for which control blueprint to use.
Please see the blueprint sources below.

`location` - Where this button is located on the controls div.
Please see the locations below.

##### Control Blueprints

* `control-replay`
Displays a large replay button, which on press plays the first scene again.

* `control-pause`
Displays a play/pause button, which on press toggles video playback.

* `control-speed`
Displays a speed control button, which controls video playback speeds.
`min` - minimum speed in float, when slider is at its lowest position.
`max` - maximum speed in float, when slider is at its highest position.
`mid` - the speed that lies in the middle of the slider, and as a default value.

* `control-volume`
Displays a volume control button, which controls audio volume.
`min` - minimum speed in float, when slider is at its lowest position.
`max` - maximum speed in float, when slider is at its highest position.
`mid` - the speed that lies in the middle of the slider, and as a default value.

* `control-hide`
Displays a hide button, which on press toggles the control visibility.

* `control-fullscreen`
Displays a fullscreen button, which on press toggles fullscreen on or off.

* `button`
Displays a button with a custom text.
`text` - set the button text
`class` - set the button CSS class
`scene` - set the scene to travel to when the button is pressed

##### Control Locations

The location can be any cardinal direction, as any of the following:
`n`,`ne`,`e`,`se`,`s`,`sw`,`w`,`nw`

Or center, denoted by:
`c`

Example usage:
```
...
"location": "nw"
...
```

#### `config.thumb` - Thumbnail

The thumb overrides the thumbnail that is by default the first frame of the first scene.
This thumbnail will be shown during the loading stage, and faded out
after loading has been finished.

Example usage:
```
"thumb": "./media/thumb.jpg"
```

#### `config.bg` - Background

The thumb overrides the black background that is left when the video is "letterboxed".
You need to specify a path to an image file that is displayed instead.

Example usage:
```
"bg": "./media/bg.jpg"
```

#### `config.cover` - Cover/Contain Letterboxing

The cover option makes the content cover the entire page instead of being
contained (or "letterboxed") as a regular video element would.
This also applies to the thumbnail media provided.

You can simply set this to true to cover the entire page.
```
"cover": true
```
This will keep the center of the image in the middle.

However, in case you need to change where the center of the image is,
you can set that using the cover option as X and Y percentage.
For example, to set the center to 10% from the top and in the middle of the image,
you can do:
```
"cover": "50% 10%"
```

#### `config.blur` - Blur Effect

The blur option determines whether the loading background should be blurred.
This also means that the thumbnail will be blurred.
There is a blur effect animation instead of the default fade in, but as this
is an expensive operation, it will not be enabled for mobile devices.

### Options for the `scenes` section

The scenes section contains information about the structure of your interactive video.
The "main" scene is played first.

Media elements can be stacked where appropriate.
For example, in order to play a queue of videos, you need to "play" the queue itself.
This allows for complex hierarchical structures that both the player and humans can understand.

Below you can find a list of all the media elements in more detail.

#### Action

Sends a play/stop action to any media element id.

Example usage:
```
{ "action": "element", "type": "play" },
{ "action": "element2", "type": "stop" }
```

#### All

Plays multiple media elements at once.

Example usage:
```
{
	"all": [
		{ "video": "./media/video.mp4" },
		{ "audio": "./media/audio.mp3" },
	]
}
```

#### Audio

Plays a specific audio file.

Example usage:
```
{ "audio": "./music/song.mp3", "volume": 0.8 }
```

Properties:

`volume` - Relative volume multiplier. Default value is 1.

`speed` - Relative speed multiplier. Default value is 1.

#### Button

Displays a button on screen.
This can be used to play the next media element in queue if used with the `wait` property,
on its own as a scene changer with the `goto` element, or as an action executor.

Centered button:
```
{ "button": "Press Me!", "anchor": "c" }
```

As a wait element:
```
{ "image": "...", "wait": { "button": "Next" } }
```

Properties:

`anchor` - Location to add the button to. Can be any of the config locations.

`key` - Which key to bind to. Can be a list of key names, contained in keycodes.js. For example, "RIGHT" is the right arrow key, "PAGEUP" is page up.

#### Delay

Does nothing for an amount of seconds.
This is useful to add delays to certain elements such as images or queues.

Example usage:
```
{ "delay": 1.0 }
```

#### Goto

Switches scenes.
In case you want to switch instantly, you can use the `instant` property.
Otherwise the scene switch will wait until all media has come to a graceful stop.

If you need to start a background scene, for example for ambient background audio, use the Play element instead.

Example usage:
```
{ "goto": "scene2" }
```

Properties:

`instant` - Force stop all playing elements and switch immediately.

#### Image

Shows an image on screen.
This will be shown indefinitely if the `wait` property is not used.

Example usage:
```
{ "image": "./media/slide.png", "wait": { "delay": 1.5 } }
```

Properties:

`wait` - Media element to wait for until we can stop.

#### Loop

Loops a media element.
This is done indefinitely unless a `wait` property is supplied.

Example usage:
```
{ "loop": { "video": "./media/loop.mp4" } }
```
```
{ "loop": { "audio": "./media/loop.mp3" }, "wait": { "button": "Stop" } }
```

Properties:

`wait` - Media element to wait for until we can stop.

#### Play

Plays a scene in the background.
This "forks" the given scene outside of the scene structure,
causing it to not be concerned about any scene changes in the main playback.

This also means that the scene(s) that are played are not stopped when scenes change.
You will need to manually invoke this by using the Stop element.

Example usage:
```
{ "play": "ambience" }
```

#### Queue

Plays multiple elements in a sequence.

Example usage:
```
{
	"queue": [
		{ "video": "./media/video1.mp4" },
		{ "video": "./media/video2.mp4" },
		{ "video": "./media/video3.mp4" },
		{ "goto": "done" }
	]
}
```

#### Random

Plays an element at random.
You can set a `ratio` key to play the elements in, giving some a better chance than others.
For example, a ratio of `[ 10, 1 ]` would play the first element roughly 10 times as much as the second element.

Example usage:
```
{
	"random": [
		{ "video": "./media/one.mp4" },
		{ "video": "./media/two.mp4" },
		{ "video": "./media/three.mp4" }
	]
}
```

With ratio:
```
{
	"random": [
		{ "video": "./media/common.mp4" },
		{ "video": "./media/rare.mp4" },
		{ "video": "./media/rarest.mp4" }
	],
	"ratio": [ 100, 10, 1 ]
}
```

Properties:

`ratio` - Which ratio to play which element in. This is passed as a list of numbers.

#### Sync

Synchronizes two elements to play back simultaneously.
This is useful for example when syncing audio/video in a looping scene.

Whenever the element to be synced with stops, the other element will be stopped as well,
regardless of whether it was finished or not.

When synchronizing audio and video, keep in mind that you probably want the audio to
synchronize *with* the video - the video will determine when the audio should stop/play.
This prevents any chopping of the video in case the audio clip is shorter/longer than
the video.

Example usage:
```
{
	"sync": { "audio": "./media/clip1.ogg" },
	"with": { "video": "./media/clip1.mp4" }
}
```

Properties:

`with` - The leading media element that determines when the other element should stop/play.

#### Stop

Stops a background scene started earlier with the "Play" element.

Example usage:
```
{ "stop": "ambience" }
```

#### Video

Plays a specific video file.
Keep in mind that for web security reasons the video plays as muted.
If you need audio, you should use the audio element instead.

Example usage:
```
{ "video": "./media/scene.mp4" }
```

Properties:

`opacity` - Relative opacity multiplier. Default value is 1.

`speed` - Relative speed multiplier. Default value is 1.

### Fade property

Some media elements can be faded in/out using the `fade` property.

In case of `audio`, the `fade` property affects the volume.
In case of `video` and `image`, the `fade` property affects the opacity.

Example usage:
```
{ "audio": "./media/audio.mp3", "fade": "5%" }
```

Instead of percentages, you can also use seconds.
```
{ "audio": "./media/audio.mp3", "fade": 2 },
{ "audio": "./media/audio.mp3", "fade": 0.5 }
```

You can also define the fade in and out separately.
```
{ "video": "./media/video.mp4", "fade": { "in": "10%", "out": "20%" } }
```

## Developer Set-Up

If you want to contribute, feel free to clone the repository and mess around with the code.
Development is mostly done on Linux, if you want to develop on Windows do get in touch and we can figure out how that is handled.

You will need Ruby to test JSON and compile releases.
For JavaScript compilation you will also need [https://www.npmjs.com/package/uglify-js](uglifyjs).
You can compile debug releases (`make debug`) without this.

The CSS is generated from SASS files using the [https://sass-lang.com/install](sass) command line executable.
Some features also require [https://www.ffmpeg.org/](ffmpeg) in order to generate thumbnails and resize media content.

CLIP uses icons from Bootstrap that are licensed under the MIT license, and a few fonts that are licensed under the Apache license.

### Testing

CLIP has both unit tests for individual JavaScript files and integration tests inside `tests/`.

You can run these using [https://gitlab.com/ilmikko/tester](tester) - CLIP doesn't currently utilize any other testing framework (that's on the long list of TODOs).

If you make a change, make sure the modules you have affected return green, and that functionality as described in integration tests stays intact.

## Feature list TODO:

* Procedural loading
* Mobile audio support
* Multiple sources generated by ffmpeg when compiling
* Picture in picture
* Varying speeds in scenes / loops
