global.assert = require('assert');
global.fs = require('fs');

console.error = function(err){ throw new Error(err); }

global.Node = function Node(tag){
	this.tagName = tag;
	this.style = {};
	this.children = [];
}
global.Node.prototype = {
	addEventListener: function(name, callback) {
		if (!this.events[name]) this.events[name] = [];
		this.events[name].push(callback);
	},
	removeEventListener: function(name, callback) {
		this.events[name].splice(this.events[name].indexOf(callback), 1);
	},
	appendChild: function(node) { this.children.push(node); },
	classList: {
		list: [],
		add: function() { Array.prototype.push.apply(this.list, arguments); },
		remove: function() { for (var item of arguments) { this.list.splice(this.list.indexOf(item), 1); } },
		contains: function(cl) { return this.list.includes(cl); }
	},
	events: {},
	fireEventListener: function(name) {
		if (!this.events[name]) return;
		for (evt of this.events[name]) evt();
	},
	insertBefore: function(node, child) { this.children.splice(this.children.indexOf(child), 0, node); },
	load: function() { this.fireEventListener('load'); },
	play: function() {},
	stop: function() {},
	pause: function() {},
	resume: function() {},
	setAttribute: function(key, val) {
		this[key] = val;
	},
	toString: function() {
		return '<' + this.tagName + '>';
	}
};

global.document = {
	documentElement: new Node(),
	createElement: function(tag){ return new Node(tag); }
};
global.window = global;

global.load = function(src) {
	eval.call(global, fs.readFileSync('./'+src)+'')
}
