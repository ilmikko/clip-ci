#!/usr/bin/env ruby
require 'json';

$version = "#{`git tag | tail -n 1`.strip}-#{`git rev-parse HEAD`.strip[-6..-1]}";

if ARGV.length == 0
  puts "Usage: #{__FILE__} [json [json [...]]]";
  exit 1;
end

def get_media json
  media = [];
  if json.is_a? String
    if json.start_with?"./"
      media << json;
    end
  elsif json.is_a? Array
    json.each{ |v|
      media += get_media v;
    };
  elsif json.is_a? Hash
    json.each{ |k,v|
      media += get_media v;
    };
  end
  return media;
end

def warn(*a)
  printf "[1m";
  puts *a;
  printf "[m";
end

def log(a)
  return if a.empty?
  puts a;
end

debug = false;

ARGV.select!{ |arg|
  if arg.start_with? "--"
    # Command.
    case arg
    when "--debug"
      warn "Compiling in debug mode.";
      debug = true;
    else
      puts "Unknown command: #{arg}";
      exit 1;
    end
    false
  else
    true
  end
};

ARGV.each{ |file|
  release = "#{file}-build-#{$version}";

  puts "Compiling #{file} -> #{release}";

  json = JSON.parse(File.read("#{file}"));

  # Clone the release dir.
  puts "Creating directory structure...";
  `mkdir -p "#{release}/"{js,css,font}`;
  log `cp -rvu "font" "#{release}"`;

  # Compile JavaScript.
  puts "Compiling JavaScript...";
  files = (Dir['js/source/*.js'] + Dir['js/*.js'] + Dir['js/media/*.js']).select{ |s| !s.include?'test' }.sort.join(' ');
  if debug
    warn "Warning: JavaScript compiled for debugging";
    log `cat #{files} > "#{release}/js/app.min.js"`;
  else
    log `uglifyjs --compress --mangle -- #{files} > "#{release}/js/app.min.js"`;
  end
  log `echo "// Version: #{$version}" >> "#{release}/js/app.min.js"`;

  # Compile CSS.
  puts "Compiling CSS...";
  log `sass css/app.sass > "#{release}/css/app.min.css"`;

  # Compile JSON.
  puts "Compiling JSON...";
  media = get_media json;
  media.each{ |media|
    local_media = File.dirname(file)+'/'+media;
    if !File.exists?local_media
      puts "File in #{file} does not exist: #{local_media}"
      exit 2;
    end
    dir="#{release}/"+File.dirname(media);
    `mkdir -p "#{dir}"`;
    log `cp -rvu "#{local_media}" "#{dir}"`;
  };

  # Compile HTML.
  puts "Compiling HTML...";
  html = File.read('html/index.template.html');

  # Replace CSS.
  html.gsub!(/{{css\.template}}/, "<link rel='stylesheet' type='text/css' href='css/app.min.css'>");
  # Replace JS.
  html.gsub!(/{{js\.template}}/, "<script src='js/app.min.js'></script>");

  # Miscellaneous config optimizations, that affect the HTML template directly.
  if json.key?('config')
    thumb_options = "-strip -interlace Plane -quality 80 -thumbnail 1000x1000";

    # Check if we are using blurred or not.
    if json['config'].key?('blur') && json['config']['blur']
      html.gsub!(/{{blur.template}}/, "blurred");
      blur_amount = 20;
      thumb_options << " -blur #{blur_amount}x#{blur_amount/2}";
    end

    if json['config'].key?('bg')
      # Replace BG.
      puts "Compiling Background...";
      bg = json['config']['bg'];
      bg_local = File.dirname(file)+'/'+bg;
      # We might want to compile a thumbnail of the bg here.
      case File.extname(bg_local)
      when ".jpg",".jpeg",".png",".gif"
        size = `du -b #{bg_local}`.strip.to_i;
        if size > 5e4
          puts "BG image size too large, resizing: #{size}B";
          puts "Creating a thumbnail...";
          log `convert #{bg_local} #{thumb_options} "#{release}/thumb.jpg"`
          json['config']['bg'] = bg = "./thumb.jpg";
        end
      when ".mp4",".mkv",".ogg",".avi"
        # Get first frame of the video as a thumbnail.
        puts "Creating a thumbnail from video...";
        log `convert #{bg_local}[0] #{thumb_options} "#{release}/thumb.jpg"`
        json['config']['bg'] = bg = "./thumb.jpg";
      else
        warn "Unknown background format:";
        puts "#{File.extname(bg_local)} in #{bg_local}";
        exit 4;
      end
      if json['config']['cover']; then
        json['config']['cover'] = '50% 50%' if json['config']['cover'] == true;
        fit = "cover; object-position: #{json['config']['cover']}";
      else
        fit = 'contain';
      end
      html.gsub!(/{{bg\.template}}/, "style='object-fit: #{fit}' src='#{bg}'") if !bg.nil?;
    end
  end

  # Replace JSON.
  html.gsub!(/{{json\.template}}/, "<script>Player.load(#{JSON.pretty_generate(json)});</script>");
  # Replace version.
  html.gsub!(/{{version}}/, $version);

  # Inject debugging code.
  if debug
    html.gsub!(/{{debug\.template}}/, File.read('html/debug.template.html'));
  end

  # Remove the rest of the templates.
  html.gsub!(/{{[^}]*}}/, '');
  # Remove comments.
  html.gsub!(/<!--[^>]*-->/, '');
  # Remove empty lines.
  html.gsub!(/^\s*$/, '');

  File.write("#{release}/index.html", html);
};

puts "All done!";
