# You probably want to see 99-visual for the actual test.

json=queue.json;
release=$json-release;

./test.rb $json || exit 1;
./compile-release.rb --debug $json || exit 1;

cat >> $release/index.html <<HERE 
<!--VISUAL
Queue test.
Test that the queue element works properly.
You should see 'A' for 1 second, 'B' for 1 second, then 'C' for 1 second.
VISUAL-->
HERE
