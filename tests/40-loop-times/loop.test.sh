# You probably want to see 99-visual for the actual test.

json=loop.json;
release=$json-release;

./test.rb $json || exit 1;
./compile-release.rb --debug $json || exit 1;

cat >> $release/index.html <<HERE
<!--VISUAL
Loop times test.
Test that the loop element respects the "times" property.
Loop2.mp4 should loop two times, after which an image is displayed for a second.
Then the loop should start over.
VISUAL-->
HERE
