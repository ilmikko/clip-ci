# You probably want to see 99-visual for the actual test.

json=loop-scene.json;
release=$json-release;

./test.rb $json || exit 1;
./compile-release.rb --debug $json || exit 1;

cat >> $release/index.html <<HERE 
<!--VISUAL
Loop scene test.
VISUAL-->
HERE
