wrap() {
	awk '{ gsub(/"/, "\\\"", $0); print("print(\""$0"\")"); }'
}

print_tests() {
	for test in ./*.json*build*; do
		[ -d "$test" ] || continue;
		comments=$(cat "$test/index.html" | awk '/VISUAL-->/ { p=0 } { if (p) print($0) } /<!--VISUAL/ { p=1 }');
		[ -n "$comments" ] || continue;
		title=$(echo "$comments" | head -n 1);
		cat <<-HERE
		print("<p>");
		$(echo "$comments" | wrap)
		print("</p>");
		print("<a href=\"$test/index.html\">");
		print("$title");
		print("</a><br>");
		HERE
	done
}

cat "./visual.template.html" | awk '/{{tests}}/ { '"$(print_tests)"' } /{{.*}}/ { gsub(/{{.*}}/, "", $0); } { print $0; }' > visual.html;
firefox visual.html;
