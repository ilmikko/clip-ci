# You probably want to see 99-visual for the actual test.

json=play.json;
release=$json-release;

./test.rb $json || exit 1;
./compile-release.rb --debug $json || exit 1;

cat >> $release/index.html <<HERE 
<!--VISUAL
All test.
Test that the 'all' element works properly.
VISUAL-->
HERE
