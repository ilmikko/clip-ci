# You probably want to see 99-visual for the actual test.

json=loop.json;
release=$json-release;

./test.rb $json || exit 1;
./compile-release.rb --debug $json || exit 1;

cat >> $release/index.html <<HERE 
<!--VISUAL
Loop test.
Test that the loop element loops video properly.
Loop2.mp4 should loop seamlessly forever.
VISUAL-->
HERE
