# You probably want to see 99-visual for the actual test.

json=mute.json;
release=$json-release;

./test.rb $json || exit 1;
./compile-release.rb --debug $json || exit 1;

cat >> $release/index.html <<HERE
<!--VISUAL
Mute test.
Mute the audio at the beginning.
Audio should remain muted throughout the duration of the test.
VISUAL-->
HERE
