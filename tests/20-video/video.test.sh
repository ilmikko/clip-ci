# You probably want to see 99-visual for the actual test.

json=video.json;
release=$json-release;

./test.rb $json || exit 1;
./compile-release.rb --debug $json || exit 1;

cat >> $release/index.html <<HERE 
<!--VISUAL
Video test.
Test that video plays properly.
Should play loop2.mp4 once.
VISUAL-->
HERE
