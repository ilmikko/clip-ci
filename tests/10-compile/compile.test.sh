# TODO: This test is not very good. It is not granular enough.
# When you have time, split this up into smaller chunks, testing each functionality separately.

git init || exit 1;
touch t;
git add t || exit 1;
git commit -m "initial commit" || exit 1;
git tag v1.0 || exit 1;
git tag v1.2 || exit 1;

# As the version, we want to have the latest tag, and the last 6 characters of the commit ID.
tag=$(git tag | tail -n 1);
commit=$(git rev-parse HEAD | tail -c 7);

version="$tag-$commit";
json=test_build.json;
release=$json-build-$version;

# This JSON should be valid
./test.rb $json || exit 1;

# Assert that compilation does not explode
./compile-release.rb $json || exit 1;

rm -rf .git;

echo "$release";
html=$(cat $release/index.html);
assert_find "$html" "<meta name=\"viewport\"" "Player.load({";

assert_find "$html" "<meta name=\"version\" content=\"$version\">";
# CSS and JS are loaded
assert_find "$html" "<script src='js/app.min.js'></script>" "<link rel='stylesheet' type='text/css' href='css/app.min.css'>";

# JS has data in it.
if [ "$(wc -c "$release/js/app.min.js" | awk '{ print $1 }')" -lt 1000 ]; then
	echo "app.min.js is probably broken, unless the player code fits into less than 1000 characters somehow.";
	echo "It should be around 30k characters minified.";
	exit 1;
fi

# CSS has data in it.
if [ "$(wc -c "$release/css/app.min.css" | awk '{ print $1 }')" -lt 700 ]; then
	echo "app.min.css is probably broken, unless the player code fits into less than 700 characters somehow.";
	echo "It should be around 8k characters minified.";
	exit 1;
fi

# Fonts are compiled.
! ! assert_equals "$(find "$release/font" | sort)" "$(cat <<HERE
$release/font
$release/font/glyphicons-halflings-regular.eot
$release/font/glyphicons-halflings-regular.svg
$release/font/glyphicons-halflings-regular.ttf
$release/font/glyphicons-halflings-regular.woff
$release/font/glyphicons-halflings-regular.woff2
$release/font/LICENSE
$release/font/Roboto_Condensed
$release/font/Roboto_Condensed/LICENSE.txt
$release/font/Roboto_Condensed/RobotoCondensed-BoldItalic.ttf
$release/font/Roboto_Condensed/RobotoCondensed-Bold.ttf
$release/font/Roboto_Condensed/RobotoCondensed-Italic.ttf
$release/font/Roboto_Condensed/RobotoCondensed-LightItalic.ttf
$release/font/Roboto_Condensed/RobotoCondensed-Light.ttf
$release/font/Roboto_Condensed/RobotoCondensed-Regular.ttf
HERE
)" || exit 1;

# Media is compiled, and only the required media is there.
! ! assert_equals "$(find "$release/test_media" | sort)" "$(cat <<HERE
$release/test_media
$release/test_media/a.png
$release/test_media/loop2.mp4
$release/test_media/trans1.mp4
$release/test_media/trans2.mp4
HERE
)" || exit 1;
