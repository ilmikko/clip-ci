# You probably want to see 99-visual for the actual test.

json=loop-wait.json;
release=$json-release;

./test.rb $json || exit 1;
./compile-release.rb --debug $json || exit 1;

cat >> $release/index.html <<HERE 
<!--VISUAL
Loop wait test.
Test that the loop element waits until a condition has been met.
Loop2.mp4 should loop for two seconds, then stop at the next cycle.
VISUAL-->
HERE
