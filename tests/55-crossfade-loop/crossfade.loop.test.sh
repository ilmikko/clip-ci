# You probably want to see 99-visual for the actual test.

json=crossfade.loop.json;
release=$json-release;

./test.rb $json || exit 1;
./compile-release.rb --debug $json || exit 1;

cat >> $release/index.html <<HERE 
<!--VISUAL
Crossfade loop test.
Test that we can crossfade video elements together in a loop.
VISUAL-->
HERE
