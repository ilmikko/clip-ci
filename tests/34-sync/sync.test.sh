# You probably want to see 99-visual for the actual test.

json=sync.json;
release=$json-release;

./test.rb $json || exit 1;
./compile-release.rb --debug $json || exit 1;

cat >> $release/index.html <<HERE 
<!--VISUAL
Sync test.
The 'sync' element should synchronize the play/stop of two media elements.
This test should show loop2.mp4 looping seamlessly, with Long.mp3 starting over every time a loop seam happens.
VISUAL-->
HERE
