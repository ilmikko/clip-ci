# You probably want to see 99-visual for the actual test.

json=random.json;
release=$json-release;

./test.rb $json || exit 1;
./compile-release.rb $json || exit 1;

cat >> $release/index.html <<HERE 
<!--VISUAL
Random test.
Test that the random element works properly.
You should see either 'A', 'B', 'C', 'D', or 'E'.
This should change randomly every time you refresh the page.
It should also change whenever you wait for long enough.
VISUAL-->
HERE
