# You probably want to see 99-visual for the actual test.

json=random-ratio.json;
release=$json-release;

./test.rb $json || exit 1;
./compile-release.rb $json || exit 1;

cat >> $release/index.html <<HERE 
<!--VISUAL
Random ratio test.
Test that the ratio property works properly.
You should see either 'A', 'B', 'C', 'D', or 'E'.
The ratio for showing either is 10000 : 1000 : 100 : 10 : 1.
This means you should mostly see A, sometimes B. Occasionally C.
You can some time get a D.
If you get an E, that is the equivalent of a jackpot and you get a lollipop.
VISUAL-->
HERE
