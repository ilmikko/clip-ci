# You probably want to see 99-visual for the actual test.

json=fade.json;
release=$json-release;

./test.rb $json || exit 1;
./compile-release.rb --debug $json || exit 1;

cat >> $release/index.html <<HERE 
<!--VISUAL
Fade test.
Test that the fade property works properly for both video and audio.
VISUAL-->
HERE
