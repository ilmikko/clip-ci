# You probably want to see 99-visual for the actual test.

json=button.json;
release=$json-release;

./test.rb $json || exit 1;
./compile-release.rb $json || exit 1;

cat >> $release/index.html <<HERE 
<!--VISUAL
Button test.
Test that the button element works properly.
You should see a 3x3 grid with the cardinal points on the sides, and 'c' in the center.
VISUAL-->
HERE
