# You probably want to see 99-visual for the actual test.

json=scene.json;
release=$json-release;

./test.rb $json || exit 1;
./compile-release.rb --debug $json || exit 1;

cat >> $release/index.html <<HERE 
<!--VISUAL
Scene test.
Test that we can have complex scenes and navigation between them.
Clicking a button with a letter should display that letter on screen.
VISUAL-->
HERE
