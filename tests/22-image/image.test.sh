# You probably want to see 99-visual for the actual test.

json=image.json;
release=$json-release;

./test.rb $json || exit 1;
./compile-release.rb --debug $json || exit 1;

cat >> $release/index.html <<HERE 
<!--VISUAL
Image test.
Test that images display correctly.
Should show a green 'A' indefinitely on the screen.
VISUAL-->
HERE
