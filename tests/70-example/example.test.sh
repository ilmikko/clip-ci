# You probably want to see 99-visual for the actual test.

json=example.json;
release=$json-release;

./test.rb $json || exit 1;
./compile-release.rb --debug $json || exit 1;

cat >> $release/index.html <<HERE 
<!--VISUAL
Example test.
This is a complex example that utilises many of the basics of clipci.
Test that the loading screen is blurred. (Autogeneration of thumbnails works)
Test that the video is cover instead of contain.
Test that speed controls work as expected.

Scenes should flow as follows:
intro
loop (button says "Progress")
outro
'A' image (button says "Again?")
intro
VISUAL-->
HERE
