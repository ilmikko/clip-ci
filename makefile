.PHONY: test build
all: build

build:
	@for project in build/*; do \
		[ -d "$$project" ] || continue; \
		for json in $$project/*.json; do \
			[ -f "$$json" ] || continue; \
			echo "Building $$json..."; \
			./test.rb $$json || exit 1; \
			./compile-release.rb $$json; \
		done \
	done

test:
	tester $(1) $(2) $(3)

debug:
	@for project in build/*; do \
		[ -d "$$project" ] || continue; \
		for json in $$project/*.json; do \
			[ -f "$$json" ] || continue; \
			echo "Building DEBUG $$json..."; \
			./test.rb $$json || exit 1; \
			./compile-release.rb --debug $$json; \
		done \
	done
