require('./test/lib.js');

global.setInterval = function(){};

load('js/alt.js');
load('js/fader.js');

load('js/mime.js');

load('js/source.js');
load('js/source/audio.js');

load('js/media.js');
load('js/media/audio.js');

global.Engine = {};
global.Queue = function(){};

function test_audio_fade_property() {
	var audio = new Audio({ src: './audio.mp3', fade: { in: 2, out: '2%'} });

	assert.deepStrictEqual(audio.source.fade, { in: 2, out: '2%' });

	// Simulate loading of media.
	audio.source.element.e.duration = 2;
	audio.source.prepareFade();

	assert.deepStrictEqual(audio.source.fade, { duration: 2, in: 2, out: '2%' });
}

function test_audio_fade_property_shorthand() {
	var audio = new Audio({ src: './audio.mp3', fade: '25%' });

	assert.strictEqual(audio.source.fade, '25%');

	// Simulate loading of media.
	audio.source.element.e.duration = 5;
	audio.source.prepareFade();

	assert.deepStrictEqual(audio.source.fade, { duration: 5, in: '25%', out: '25%' });
}
