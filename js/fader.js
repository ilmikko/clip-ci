/**
 * Controls fading the properties for media elements.
 * */

var Fader = (function() {
	var id = 0;
	var fades = {};

	function Fader(source, options) {
		this.id = id++;

		Fader.attach(this, source);

		this.element = source.element;
		this.duration = options.duration = this.element.e.duration;
		this.speed = 1;

		Fader.options(this, options);
	}

	Fader.prototype = {
		falloff: function(p) { return p; },

		hide: function() {
			// this.update(this.start + this.duration);
			delete fades[this.id];
		},
		show: function() {
			fades[this.id] = this;

			this.setSpeed(Engine.speed);

			this.start = Date.now() / 1000;
			this.update(this.start);
		},

		pause: function(now) {
			this.last_paused = now;
		},
		resume: function(now) {
			this.start = now - (this.last_paused - this.start);
		},

		setSpeed: function(speed, now) {
			if (this.speed == speed) return;
			var r = this.speed / speed;
			this.duration *= r;
			this.start = now - (now - this.start) * r;
			this.last_paused = now - (now - this.last_paused) * r;
			this.speed = speed;
		},

		update: function(now) {
			var p = this.falloff((now - this.start) / this.duration, this.duration);

			this.source.setFaderOpacity(p);
			this.source.setFaderVolume(p);
		}
	};

	$.extend(Fader, {
		attach: function(fader, source) {
			source.show = (function(sup) {
				return function() {
					sup.apply(this, arguments);
					fader.show();
				}
			})(source.show);

			source.hide = (function(sup) {
				return function() {
					fader.hide();
					sup.apply(this, arguments);
				}
			})(source.hide);

			fader.source = source;
		},
		tick: function() {
			if (Engine.paused) return;
			var now = Date.now() / 1000;
			for (var fade in fades) fades[fade].update(now);
		},
		options: function(fader, options) {
			var duration = options.duration;

			function parse(value) {
				if (typeof value === 'string') return (parseFloat(value) / 100) * duration;
				return value;
			}

			var left = parse(options.in);
			var right = duration - parse(options.out);

			// TODO: Revise this happening only once
			faded = false;

			fader.falloff = function(p) {
				var t = p * duration;
				// Trapezoid falloff. __/``\__
				if (t < 0) {
					return 0;
				} else if (t < left) {
					if (faded) faded = false;
					return t / left;
				} else if (t > duration) {
					faded = false;
					fader.hide();
					return 0;
				} else if (t > right) {
					if (!faded) {
						faded = true;
						fader.source.progress();
					}
					return 1 - (t - right) / (duration - right);
				} else return 1;
			}
		},

		pause: function() {
			var now = Date.now() / 1000;
			for (var fade in fades) fades[fade].pause(now);
		},
		resume: function() {
			var now = Date.now() / 1000;
			for (var fade in fades) fades[fade].resume(now);
		},

		setSpeed: function(speed) {
			var now = Date.now() / 1000;
			for (var fade in fades) fades[fade].setSpeed(speed, now);
		},
	});

	setInterval(function() {
		Fader.tick();
	}, 50);

	return Fader;
})();
