require('./test/lib.js');

load('js/alt.js');

load('js/mime.js');
load('js/source.js');
load('js/source/video.js');

function test_video_options() {
	var video = new SourceVideo('./video.mp4');
	video.config({ opacity: 0.9, speed: 0.5 });

	assert.strictEqual(video.opacity, 0.9);
	assert.strictEqual(video.speed, 0.5);
}

async function test_video_prepare() {
	var video = new SourceVideo('./video.mp4');

	// src should be not set on the element.
	assert.strictEqual(video.element.e.src, undefined);
	assert.deepStrictEqual(video.element.e.children, []);

	await new Promise(function(resolve) {
		video.prepare(function() {
			resolve();
		});

		// Prepare should assign the src and wait for the video to load.
		// This is done by a single <source> element.
		assert.strictEqual(video.element.e.children.length, 1);
		assert.strictEqual(video.element.e.children[0].tagName, 'source');
		assert.strictEqual(video.element.e.children[0].src, './video.mp4');

		// Element should be a video element
		assert.strictEqual(video.element.e.tagName, 'video');
		assert(video.element.e.classList.contains('full'), 'full not in class list for video.element');

		// The element should have these default values
		assert.strictEqual(video.element.e.muted, 'muted');
		assert.strictEqual(video.element.e.preload, 'auto');
		assert.strictEqual(video.element.e.playsinline, 'playsinline');

		// Start hidden
		assert(video.element.e.classList.contains('hidden'), 'hidden not in class list for video.element');

		// Simulate video loading here.
		setTimeout(function() {
			video.element.e.fireEventListener('canplaythrough');
		}, 10);
	});
}

// TODO: Test video preplay
