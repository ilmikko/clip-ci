require('./test/lib.js');

load('js/alt.js');

load('js/mime.js');
load('js/source.js');
load('js/source/audio.js');

global.Engine = { volume: 1 };

function test_audio_options() {
	var audio = new SourceAudio('./audio.mp3');
	audio.config({ volume: 0, speed: 0.5 });

	assert.strictEqual(audio.volume, 0);
	assert.strictEqual(audio.speed, 0.5);
}

async function test_audio_prepare() {
	var audio = new SourceAudio('./audio.mp3');
	audio.config({ volume: 0.9, speed: 0.5 });

	// src should be not set on the element.
	assert.strictEqual(audio.element.e.src, undefined);
	assert.deepStrictEqual(audio.element.e.children, []);

	await new Promise(function(resolve) {
		audio.prepare(function() {
			resolve();
		});

		// Prepare should assign the src and wait for the audio to load.
		// This is done by a single <source> element.
		assert.strictEqual(audio.element.e.children.length, 1);
		assert.strictEqual(audio.element.e.children[0].tagName, 'source');
		assert.strictEqual(audio.element.e.children[0].src, './audio.mp3');

		assert.strictEqual(audio.element.e.volume, 0.9);
		assert.strictEqual(audio.element.e.playbackRate, 0.5);

		// Simulate audio loading here.
		setTimeout(function() {
			audio.element.e.fireEventListener('canplaythrough');
		}, 10);
	});
}

// TODO: Test audio preplay
