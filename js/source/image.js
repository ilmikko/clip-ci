/**
 * Prepareable image.
 * */

var SourceImage = (function() {
	function SourceImage(srcs) {
		this.srcs = srcs;
		this.element = $('>picture')
			.addClass('full', 'hidden', 'noselect')
			.append(
				$('>img')
				.addClass('full')
				.css({ objectFit: 'contain' })
			);

		this.init();
	}

	SourceImage.prototype = Object.create(Source.prototype);

	$.extend(SourceImage.prototype, {
		preplay: function(callback, update) { update(1); callback(); },
		prepare: function(callback) {
			// Make sure we can display this image

			var self = this;
			var timeout = 5000; // milliseconds

			var fired = false;

			function done(){
				if (fired) {
					console.warn("Image callback tried to fire multiple times...");
					return;
				} else {
					fired = true;
				}
				self.element.off("error");
				if (callback) callback();
			}

			// Automatic retry as some browsers hiccup on the loading sometimes (I know)
			function retry() {
				// Wait a while until retrying
				var timeoutObject = setTimeout(function() {
					var readyState = self.element.e.readyState;

					if (readyState < 3) { 
						console.warn(self.srcs + " is taking a while to load. (readyState=" + readyState + ")");
						retry();
					} else {
						console.warn(self.srcs + " skipped preplay...");
						done();
					}
				}, timeout);

				self.element
					.on("error", function(err) {
						clearTimeout(timeoutObject);
						if (Player) Player.error("Image prepare error " + err);
					})
					.one("load", function() {
						clearTimeout(timeoutObject);
						done();
					});

				// Loop through our sources, appending them to the element.
				for (var g = 0, gg = self.srcs.length; g < gg; g++) {
					var source = self.srcs[g];

					// Which separator do we use in the url?
					var separator = "?";
					if (source.indexOf('?') > -1) separator = "&";

					self.element.prepend(
						$('>source').set({
							srcset: source + separator + "r=" + Math.random()
						})
					);
				}

				// Final img element for backwards compatibility.
				var separator = "?", source = self.srcs[0];
				if (source.indexOf('?') > -1) separator = "&";

				self.element.$('img')
					.set({
						src: source + separator + "r=" + Math.random()
					})
					.one('load', function(){
						clearTimeout(timeoutObject);
						done();
					})
			}
			retry();
		},

		show: function() {
			this.element
				.css({ zIndex: 1 })
				.removeClass('hidden');
		},
		hide: function() {
			var self = this;

			this.element.css({ zIndex: 0 });

			setTimeout(function() {
				if (self.active) return;
				self.element.addClass('hidden');
			}, 100);
		},

		play: function() {
			this.active = true;
			this.show();
		},
		stop: function() {
			this.active = false;
			this.hide();
		},

		pause: function() {},
		resume: function() {},
	});

	return SourceImage;
})();
