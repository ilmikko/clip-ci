/**
 * Prepareable / preplayable HTML5 audio.
 * */

var SourceAudio = (function() {
	function SourceAudio(srcs) {
		this.srcs = $.wrap(srcs);
		this.element = $('>audio')
			.prop({ preload: 'auto', muted: 'muted' });

		this.init();
	}

	SourceAudio.prototype = Object.create(Source.prototype);

	$.extend(SourceAudio.prototype, {
		play: function() {
			this.active = true;

			this.setEngineOpacity(Engine.opacity);
			this.setEngineVolume(Engine.volume);
			this.setEngineSpeed(Engine.speed);

			var self = this;
			this.element
				.prop({ muted: false, currentTime: 0, volume: this.volume, playbackRate: this.speed })
				.off('ended')
				.one('ended', function() { self.progress(); })
				.play();

			this.show();
		},
		stop: function() {
			this.active = false;
			this.element.pause();
			this.hide();
		},

		pause: function() {
			this.element.pause();
		},
		resume: function() {
			if (!this.active) return;
			this.element.play();
		},
	});

	return SourceAudio;
})();
