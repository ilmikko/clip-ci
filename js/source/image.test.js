require('./test/lib.js');

load('js/alt.js');

load('js/mime.js');
load('js/source.js');
load('js/source/image.js');

async function test_image_prepare() {
	var image = new SourceImage('./image.png');

	assert.strictEqual(image.element.e.src, undefined);
	assert.strictEqual(image.element.e.children.length, 1);
	assert.strictEqual(image.element.e.children[0].tagName, 'img');
	assert.strictEqual(image.element.e.children[0].src, undefined);

	// Element should be a picture element
	assert.strictEqual(image.element.e.tagName, 'picture');
	assert(image.element.e.classList.contains('full'), 'full not in class list for image.element');

	// Start hidden
	assert(image.element.e.classList.contains('hidden'), 'hidden not in class list for image.element');
}
