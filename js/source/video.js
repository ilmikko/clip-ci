/**
 * Prepareable / preplayable HTML5 video.
 * */

var SourceVideo = (function() {
	function SourceVideo(srcs) {
		this.srcs = $.wrap(srcs);
		this.element = $('>video')
			.addClass('full', 'hidden', 'noselect')
			.prop({ preload: 'auto', playsinline: 'playsinline', muted: 'muted' })
			.on("contextmenu", function(evt){
				evt.preventDefault();
				return false;
			});

		this.init();
	}

	SourceVideo.prototype = Object.create(Source.prototype);

	$.extend(SourceVideo.prototype, {
		show: function() {
			this.setEngineOpacity(Engine.opacity);

			this.element
				.css({ zIndex: 1, opacity: this.opacity })
				.removeClass('hidden');
		},
		hide: function() {
			// We want to set a timeout for hiding the video to prevent flashing.
			var self = this;

			this.element.css({
				zIndex: 0,
			});

			setTimeout(function() {
				if (self.active) return;

				self.element
					.prop({ currentTime: 0 })
					.addClass('hidden')
					.pause();
			}, 20);
		},

		play: function() {
			this.active = true;

			this.setEngineSpeed(Engine.speed);

			var self = this;

			this.element
				.css({ zIndex: 10 })
				.prop({ currentTime: 0, playbackRate: this.speed })
				.off('ended')
				.one('ended', function() { self.progress(); })
				.play();

			this.show();
		},
		stop: function() {
			this.active = false;
			this.element.pause();
			this.hide();
		},

		pause: function() {
			this.element.pause();
		},
		resume: function() {
			if (!this.active) return;
			this.element.play();
		},
	});

	return SourceVideo;
})();
