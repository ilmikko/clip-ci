/**
 * Console helper for people that are unable to access the browser console.
 * */

var Console = (function (){
	var log = [];
	console.log = (function(sup){
		return function(){
			sup.apply(this, arguments);
			Array.prototype.push.apply(log, arguments);
		}
	})(console.log);
	console.warn = (function(sup){
		return function(){
			sup.apply(this, arguments);
			log.push("WARNING:");
			Array.prototype.push.apply(log, arguments);
		}
	})(console.warn);
	console.error = (function(sup){
		return function(){
			sup.apply(this, arguments);
			log.push("ERROR:");
			Array.prototype.push.apply(log, arguments);
		}
	})(console.error);

	return {
		log: log,
		display: function() {
			for (var g = 0, gg = log.length; g < gg; g++) {
				if (typeof log[g] === 'object') log[g] = JSON.stringify(log[g]);
			}

			$(document.body).append(
				$(">div")
				.css({
					position: 'absolute',
					overflow: 'auto',
					left: 0,
					top: 0,
					width: '100%',
					height: '50%',
					zIndex: 1000,
				})
				.html(log.join("\n<br>"))
			);
		}
	};
})();
