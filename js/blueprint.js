// A lightweight blueprinting engine for the scene / persistent controls.
var blueprint = (function () {
	var blueprints = {
		'control-replay': function () {
			return $('>button')
				.addClass('icon-text', 'large', 'scene')
				.set({ name: 'replay' })
				.text('Replay?')
				.on('click', function () {
					this.remove();
					Player.replay();
				});
		},
		'control-pause': function() {
			return $('>button')
				.addClass('control-pause')
				.append(
					$('>span').class('icon').set({ name: 'pause' })
				)
				.on('click', function(evt) {
					evt.stopPropagation();
					Player.togglePaused();
				})
				.on('keyup', function(evt) {
					evt.preventDefault();
				});
		},
		'control-speed': function (o) {
			// Revised speed control...

			var slider = verticalSlider({
				min: o.min || 0.5,
				mid: o.mid || 1,
				max: o.max || 3
			})
				.on('change', function () {
					Engine.setSpeed(this.value());
				})
				.on('input', function () {
					Engine.setSpeed(this.value(), true);
				});

			var popup = $('>div')
				.addClass('popup', 'hidden', 'fade')
				.append(slider);

			var button = $('>button')
				.addClass('control-speed')
				.append(
					$('>span').class('icon').set({ name: 'speed' })
				)
				.on('click', function (evt) {
					evt.stopPropagation();

					$("#player").$$(".popup").addClass('hidden');

					// Show the popup
					popup.removeClass('hidden');

					// But hide it as soon as we click somewhere else
					$(document.body).one('click', function () {
						popup.addClass('hidden');
					});
				});

			return $('>div')
				.addClass('button-popup')
				.append(
					popup,
					button
				);
		},
		'control-volume': function (o) {
			var icon = $('>span').class('icon').set({ name: 'volume' });

			function updateIcon(value) {
				if (value > 0.66) {
					icon.set({ name: 'volume' });
				} else if (value !== 0) {
					icon.set({ name: 'volume-50' });
				} else {
					icon.set({ name: 'volume-muted' });
				}
			}
			// Start with the right icon
			updateIcon(o.mid);

			var slider = verticalSlider({
				min: o.min || 0,
				mid: o.mid || 0.5,
				max: o.max || 1
			})
				.on('change', function () {
					Engine.setVolume(this.value());
				})
				.on('input', function () {
					var value = this.value();
					updateIcon(value);
					Engine.setVolume(value, true);
				});

			var popup = $('>div')
				.addClass('popup', 'hidden', 'fade')
				.append(slider);

			var mute_volume = 1; // A place to store the volume before muting

			var button = $('>button')
				.addClass('control-volume')
				.append(
					icon
				)
				.on('click', function (evt) {
					evt.stopPropagation();

					$("#player").$$(".popup").addClass('hidden');

					// Show the popup
					popup.removeClass('hidden');

					// But hide it as soon as we click somewhere else
					$(document.body).one('click', function () {
						popup.addClass('hidden');
					});
				})
				.on('dblclick', function (evt) {
					evt.stopPropagation();

					// Hide popup
					popup.addClass('hidden');

					// Mute / unmute
					var value = slider.value();
					if (value === 0) {
						slider.dragTo(mute_volume);
					} else {
						mute_volume = value;
						slider.dragTo(0);
					}

				});

			slider.dragTo(Engine.volume);

			return $('>div')
				.addClass('button-popup')
				.append(
					popup,
					button
				);
		},
		'control-hide': function () {
			return $('>button')
				.addClass('control-hide')
				.append(
					$('>span').class('icon').set({ name: 'hide' })
				)
				.on('click', function (evt) {
					evt.stopPropagation();
					Player.toggleControls();
				})
				.on('keyup', function(evt) {
					evt.preventDefault();
				});
		},
		'control-fullscreen': function () {
			return $('>button')
				.addClass('control-fullscreen')
				.append(
					$('>span').class('icon').set({ name: 'fullscreen' })
				)
				.on('click', function (evt) {
					evt.stopPropagation();
					Player.toggleFullscreen();
				})
				.on('keyup', function(evt) {
					evt.preventDefault();
				});
		},
		'button': function (o) {
			o = o || {};

			var button = $('>button')
				.addClass('scene')
				.css({ opacity: 0 })
				.prop({ disabled: true });

			var text = o.text || '';

			button.text(text);

			setTimeout(function () {
				button.css({ opacity: 1 }).prop({ disabled: false });
			}, o.delay || 150);

			return button;
		}
	};

	function build(control) {
		var type = control.src;
		if (!(type in blueprints)) throw new Error("Cannot find blueprint `" + type + "`!");
		return blueprints[type](control);
	}

	var public = {
		build: build
	};

	return public;
})();
