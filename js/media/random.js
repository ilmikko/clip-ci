/**
 * Play any media element at random.
 * */

function Random(o) {
	o = o || {};

	var src = $.wrap(o.src) || [];

	for (var g = 0, gg = src.length; g < gg; g++) src[g] = Media.create(src[g]);

	this.children = this.src = src;

	var ratio = o.ratio;
	if (ratio == null) { ratio = []; for (var g = 0, gg = src.length; g < gg; g++) ratio.push(1); }

	if (ratio.length != this.src.length) {
		throw new Error("Ratio length does not match the media length! (" + ratio.length + "!=" + this.src.length + ")");
	}

	this.ratio = ratio;

	this.ratio_sum = 0;
	for (var g = 0, gg = ratio.length; g < gg; g++) this.ratio_sum += ratio[g];

	this.pick();
}

Random.prototype = Object.create(Media.prototype);

$.extend(Random.prototype, {
	pick: function() {
		// Pick a new random element to play, based on the ratios.
		var rand = Math.random() * this.ratio_sum;

		for (var g = 0, gg = this.ratio.length; g < gg; g++) {
			if ((rand -= this.ratio[g]) < 0) {
				this.index = g;
				break;
			}
		}
	},

	play: function() {
		// Pick a random element to play.
		var self = this;
		this.src[this.index].stop();
		this.pick();
		this.src[this.index].next = function(){
			self.progress();
		};
		this.src[this.index].play();
	},
	stop: function() {
		this.src[this.index].stop();
	}
});
