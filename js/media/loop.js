/**
 * Loops a media element.
 * */

function Loop(o) {
	o = o || {};

	this.current = 0;
	this.max = o.times || -1;

	this.src = Media.create(o.src);
	this.children = [ this.src ];
}

Loop.prototype = Object.create(Media.prototype);

$.extend(Loop.prototype, {
	play: function() {
		var self = this;
		this.current = 0;
		(function loop() {
			self.current++;
			if (self.max > 0 && self.current > self.max) {
				self.progress();
				self.src.stop();
				return;
			}
			self.src.next = function() { loop(); };
			self.src.play();
		})();
	},
	stop: function() {
		var self = this;
		this.src.next = function() { self.progress(); };
		this.src.stop();
		this.current = 0;
	},
});
