/**
 * Basic delay.
 * Plays nothing for x seconds and stops.
 * */

function Delay(o) {
	o = o || {};

	this.delay = o.src || 1;
}

Delay.prototype = Object.create(Media.prototype);

$.extend(Delay.prototype, {
	play: function() {
		var self = this;
		this.timeout = new Timeout(function() { self.progress(); }, this.delay);
	},
	hide: function() {
		this.timeout.stop();
	},
});
