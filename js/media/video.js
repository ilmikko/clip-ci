/**
 * Media wrapper around the Video source.
 */

function Video(o) {
	o = o || {};

	this.source = Source.video(o.src);
	this.source.config(o);
}

Video.prototype = Object.create(Media.prototype);

$.extend(Video.prototype, {
	getSources: function() { return this.sources = [ this.source ]; },

	play: function() {
		var self = this;

		if (this.source.fade) {
			this.source.next = function() { self.progress(); };
		} else {
			this.source.next = function() { self.source.stop(); self.hide(); self.progress(); };
		}
		this.source.play();
	},
	stop: function() {
		if (!this.source.active) this.progress();
	},
	hide: function() {
		this.source.hide();
	},
});
