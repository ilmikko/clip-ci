/**
 * Show an image on the screen.
 * The wait property is used to tell when to stop displaying the image.
 * */

function Image(o) {
	o = o || {};

	this.source = Source.image(o.src);
	this.source.config(o);
}

Image.prototype = Object.create(Media.prototype);

$.extend(Image.prototype, {
	getSources: function() { return this.sources = [ this.source ]; },
	play: function() {
		this.source.play();
	},
	stop: function() {
		this.source.stop();
		this.progress();
	},
	hide: function() {
		this.source.stop();
	},

	pause: function() {},
	resume: function() {},
});
