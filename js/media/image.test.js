require('./test/lib.js');

load('js/alt.js');

load('js/source.js');
load('js/source/image.js');
load('js/media.js');
load('js/media/image.js');

function test_image_media() {
	var image = new Image();

	// Check that the image has one source.
	var sources = image.getSources();
	assert.strictEqual(sources.length, 1);

	// Source element should be a picture element
	var source = sources[0];
	assert.strictEqual(source instanceof SourceImage, true);
	assert.strictEqual(source.element.e.tagName, 'picture');
}

function test_image_no_parameters() {
	var image = new Image();

	// Multipliers are by default 1
	assert.strictEqual(image.source.speed, 1);
	assert.strictEqual(image.source.volume, 1);
	assert.strictEqual(image.source.opacity, 1);

	// Start inactive
	assert.strictEqual(image.source.active, false);
}

function test_image_methods() {
	var image = new Image();

	assert.strictEqual(typeof(image.play), 'function');
	assert.strictEqual(typeof(image.stop), 'function');

	assert.strictEqual(typeof(image.resume), 'function');
	assert.strictEqual(typeof(image.pause), 'function');

	assert.strictEqual(typeof(image.setEngineSpeed), 'function');
	assert.strictEqual(typeof(image.setEngineVolume), 'function');
	assert.strictEqual(typeof(image.setEngineOpacity), 'function');
}

function test_image_stop_progression() {
	var image = new Image();

	var progressed = false;

	image.next = function() { progressed = true; }

	assert.strictEqual(progressed, false);

	image.stop();

	assert.strictEqual(progressed, true);
}
