/**
 * Media wrapper around the Audio source.
 */

function Audio(o) {
	o = o || {};

	this.source = Source.audio(o.src);
	this.source.config(o);
}

Audio.prototype = Object.create(Media.prototype);

$.extend(Audio.prototype, {
	getSources: function() { return this.sources = [ this.source ]; },

	play: function() {
		var self = this;
		this.source.next = function() { self.progress(); };
		this.source.play();
	},
	stop: function() {
		this.source.stop();
	},
	hide: function() {
		this.source.stop();
	}
});
