/**
 * Plays multiple media elements back to back.
 * */

function Queue(o) {
	o = o || {};

	var src = $.wrap(o.src) || [];

	for (var g = 0, gg = src.length; g < gg; g++) src[g] = Media.create(src[g]);

	this.children = this.src = src;
	this.index = 0;
}

Queue.prototype = Object.create(Media.prototype);

$.extend(Queue.prototype, {
	play: function() {
		var self = this;
		this.src[this.index].next = function() {
			self.index = (self.index + 1) % self.src.length;

			if (self.index == 0) {
				self.progress();
				return;
			}

			self.play();
		};
		this.src[this.index].play();
	},
	stop: function() {
		var self = this;
		this.src[this.index].next = function() { self.progress(); };
		this.src[this.index].stop();
		this.index = 0;
	},
});
