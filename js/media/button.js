/**
 * Displays a button on screen.
 * */

function Button(o) {
	o = o || {};

	this.text = o.src;
	this.anchor = $('#player .controls').$('.controls-' + (o.anchor || "s"));
	this.src = blueprint.build({
		src: 'button',
		text: this.text
	});

	if (o.key) {
		this.src.e.dataset.keyCode = KeyCode.GetCode(o.key);
	}
	if (o.class) this.src.class(o.class);
}

Button.prototype = Object.create(Media.prototype);

$.extend(Button.prototype, {
	hide: function() {
		this.src.remove();
	},
	show: function() {
		this.src
			.off('click')
			.removeClass('disabled')
			.css({ opacity: 1 });

		this.anchor.append(this.src);
	},

	play: function() {
		this.show();

		var self = this;
		this.src.on('click', function() {
			if (Engine.paused) return;
			self.stop();
		});
	},
	stop: function() {
		// Style this button so people know the press has registered.
		this.src.addClass('disabled').css({ opacity: 0.4 });
		this.progress();
		this.hide();
	}
});
