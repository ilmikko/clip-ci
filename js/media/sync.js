/**
 * Synchronizes the play and pause of two media elements.
 * */

function Sync(o) {
	o = o || {};

	this.src = Media.create(o.src);
	this.master = Media.create(o.with);

	this.children = [ this.src, this.master ];
}

Sync.prototype = Object.create(Media.prototype);

$.extend(Sync.prototype, {
	play: function() {
		var self = this;
		this.master.next = function() { self.stop(); self.progress(); };
		this.master.play();
		this.src.play();
	},
	stop: function() {
		var self = this;
		this.master.next = function() {
			self.src.next = function() {};
			self.src.stop();
			self.src.hide();
			self.progress();
		};
		this.master.stop();
	},
});
