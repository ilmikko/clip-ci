/**
 * Plays media elements simultaneously until a condition finishes.
 * When the wait condition finishes, all of the media elements finish.
 * */

function All(o) {
	o = o || {};

	var src = $.wrap(o.src) || [];

	for (var g = 0, gg = src.length; g < gg; g++) src[g] = Media.create(src[g]);

	this.src = src;
	if (o.until) this.wait = Media.create(o.until);

	this.children = src;
	if (this.wait) this.children.push(this.wait);
}

All.prototype = Object.create(Media.prototype);

$.extend(All.prototype, {
	play: function() {
		var self = this;

		if (this.wait) {
			this.wait.next = function() {
				self.stop();
			};
			this.wait.play();
		} else {
			var playing = new LoadingHandler('PLAYING');

			playing.on('finish', function() { self.progress(); });

			for (var g = 0, gg = this.src.length; g < gg; g++) {
				playing.start(g);
				this.src[g].next = (function(g) {
					return function() { playing.end(g); }
				})(g);
			}

			playing.fire();
		}

		for (var g = 0, gg = this.src.length; g < gg; g++) this.src[g].play();
	},
	stop: function() {
		var stopping = new LoadingHandler('STOPPING');

		var self = this;
		stopping.on('finish', function() { self.progress(); });

		if (this.wait) {
			stopping.start('wait');
			this.wait.next = function() { stopping.end('wait'); };
			this.wait.stop();
		}
		for (var g = 0, gg = this.src.length; g < gg; g++) {
			stopping.start(g);
			this.src[g].next = (function(g) {
				return function() { stopping.end(g); }
			})(g);
			this.src[g].stop();
		}

		stopping.fire();
	},
});
