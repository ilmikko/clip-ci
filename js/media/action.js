// This class entails actions that happen to specific IDs of media.
function Action(o) {
	o = o || {};

	var actions = {
		stop: function stop(id, callback) { Engine.stop(id, callback); },
		play: function play(id, callback) { Engine.play(id, callback); }
	};

	if (!(o.type in actions)) throw new Error("Invalid action: '"+o.type+"'");

	this.action = actions[o.type];
	this.src = o.src;
}
Action.prototype = Object.create(Media.prototype);
$.extend(Action.prototype, {
	play: function() {
		var self = this;
		console.log("ACTIoN ON " + this.src);
		this.action(this.src, function() {
			self.progress();
		});
	}
});
