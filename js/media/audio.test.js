require('./test/lib.js');

load('js/alt.js');

load('js/source.js');
load('js/source/audio.js');
load('js/media.js');
load('js/media/audio.js');

global.Engine = { volume: 1 };

function test_audio_media() {
	var audio = new Audio();

	// Check that the audio has one source.
	var sources = audio.getSources();
	assert.strictEqual(sources.length, 1);

	// Source element should be a audio element
	var source = sources[0];
	assert.strictEqual(source instanceof SourceAudio, true);
	assert.strictEqual(source.element.e.tagName, 'audio');
}

function test_audio_no_parameters() {
	var audio = new Audio();

	// Multipliers are by default 1
	assert.strictEqual(audio.source.speed, 1);
	assert.strictEqual(audio.source.volume, 1);
	assert.strictEqual(audio.source.opacity, 1);

	// Start inactive
	assert.strictEqual(audio.source.active, false);
}

function test_audio_methods() {
	var audio = new Audio();

	assert.strictEqual(typeof(audio.play), 'function');
	assert.strictEqual(typeof(audio.stop), 'function');

	assert.strictEqual(typeof(audio.resume), 'function');
	assert.strictEqual(typeof(audio.pause), 'function');

	assert.strictEqual(typeof(audio.setEngineSpeed), 'function');
	assert.strictEqual(typeof(audio.setEngineVolume), 'function');
	assert.strictEqual(typeof(audio.setEngineOpacity), 'function');
}

function test_audio_stop_progression() {
	var audio = new Audio();

	var progressed = false;

	audio.next = function() { progressed = true; }
	audio.play();

	assert.strictEqual(progressed, false);

	audio.stop();

	// Progression waits for video to end when stopping.
	assert.strictEqual(progressed, false);

	audio.source.element.e.fireEventListener('ended');

	assert.strictEqual(progressed, true);
}
