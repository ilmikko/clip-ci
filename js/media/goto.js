/**
 * Go to a scene.
 * */

function Goto(o) {
	this.src = o.src;
	this.instant = o.instant;
}

Goto.prototype = Object.create(Media.prototype);

$.extend(Goto.prototype, {
	play: function() {
		// Go to a scene.
		console.log("Goto:", this.src);

		if (this.instant) {
			Engine.hide(this.scene);
			Engine.stop(this.scene);
			Engine.play(this.src, function(){});
			return;
		}

		var self = this;
		Engine.stop(this.scene, function() {
			console.log("Playing: ", self.src);
			Engine.play(self.src, function(){});
		});
	},
	stop: function() { this.progress(); },
});
