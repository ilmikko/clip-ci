require('./test/lib.js');

load('js/alt.js');

load('js/source.js');
load('js/source/video.js');
load('js/media.js');
load('js/media/video.js');

global.Engine = {};

function test_video_media() {
	var video = new Video();

	// Check that the video has one source.
	var sources = video.getSources();
	assert.strictEqual(sources.length, 1);

	// Source element should be a video element
	var source = sources[0];
	assert.strictEqual(source instanceof SourceVideo, true);
	assert.strictEqual(source.element.e.tagName, 'video');
}

function test_video_no_parameters() {
	var video = new Video();

	// Multipliers are by default 1
	assert.strictEqual(video.source.speed, 1);
	assert.strictEqual(video.source.volume, 1);
	assert.strictEqual(video.source.opacity, 1);

	// Start inactive
	assert.strictEqual(video.source.active, false);
}

function test_video_methods() {
	var video = new Video();

	assert.strictEqual(typeof(video.play), 'function');
	assert.strictEqual(typeof(video.stop), 'function');

	assert.strictEqual(typeof(video.pause), 'function');
	assert.strictEqual(typeof(video.resume), 'function');

	assert.strictEqual(typeof(video.setEngineSpeed), 'function');
	assert.strictEqual(typeof(video.setEngineVolume), 'function');
	assert.strictEqual(typeof(video.setEngineOpacity), 'function');
}

function test_video_stop_progression() {
	var video = new Video();

	var progressed = false;

	video.next = function() { progressed = true; }
	video.play();

	assert.strictEqual(progressed, false);

	video.stop();

	// Progression waits for video to end when stopping.
	assert.strictEqual(progressed, false);

	video.source.element.e.fireEventListener('ended');

	assert.strictEqual(progressed, true);
}
