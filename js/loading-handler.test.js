require('./test/lib.js');

load('js/loading-handler.js');

function test_no_fire() {
	var loading = new LoadingHandler('ID');

	loading.on('finish', function() {
		throw new Error('Loading handler finished');
	});
}

function test_empty_pending() {
	var loading = new LoadingHandler('ID');

	var finished = false;

	loading.on('finish', function() { finished = true; });

	assert.strictEqual(finished, false);

	loading.fire();

	assert.strictEqual(finished, true);
}

function test_regular_case() {
	var loading = new LoadingHandler('ID');

	var finished = false;

	loading.on('finish', function() { finished = true; });

	assert.strictEqual(loading.pending, 0);
	assert.strictEqual(finished, false);

	loading.start('1');

	assert.strictEqual(loading.pending, 1);
	assert.strictEqual(finished, false);

	loading.start('2');

	assert.strictEqual(loading.pending, 2);
	assert.strictEqual(finished, false);

	loading.end('2');

	assert.strictEqual(loading.pending, 1);
	assert.strictEqual(finished, false);

	loading.start('3');

	assert.strictEqual(loading.pending, 2);
	assert.strictEqual(finished, false);

	loading.end('1');

	assert.strictEqual(loading.pending, 1);
	assert.strictEqual(finished, false);

	loading.end('3');

	loading.fire();

	assert.strictEqual(loading.pending, 0);
	assert.strictEqual(finished, true);
}

function test_premature_finish() {
	var loading = new LoadingHandler('ID');

	var finished = false;

	loading.on('finish', function() { finished = true; });

	assert.strictEqual(loading.pending, 0);
	assert.strictEqual(finished, false);

	loading.start('1');

	assert.strictEqual(loading.pending, 1);
	assert.strictEqual(finished, false);

	loading.start('2');

	assert.strictEqual(loading.pending, 2);
	assert.strictEqual(finished, false);

	loading.end('2');

	assert.strictEqual(loading.pending, 1);
	assert.strictEqual(finished, false);

	loading.end('1');

	assert.strictEqual(loading.pending, 0);
	assert.strictEqual(finished, false);

	loading.start('3');

	assert.strictEqual(loading.pending, 1);
	assert.strictEqual(finished, false);

	loading.end('3');

	loading.fire();

	assert.strictEqual(loading.pending, 0);
	assert.strictEqual(finished, true);
}
