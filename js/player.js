var Player = (function() {
	var $player = null;

	var controllocations = { 'nw': 1, 'n': 1, 'ne': 1, 'w': 1, 'c': 1, 'e': 1, 'sw': 1, 's': 1, 'se': 1 };

	function error(msg) {
		console.error('Playback error: ' + msg);
		hideLoading();
		showError('Playback Error :<', msg.toString());
		if (Console) Console.display();
	}

	// Attach a control element to the player
	function attach_control(element, location) {
		if (!location) location = 's';
		if (!(location in controllocations)) throw new Error("Cannot parse location `" + location + "`, not a valid location!");
		$player.$('.controls-' + location).append(element);
	}

	// Attach the player element
	function attach(playerElement) {
		// Attach player to this specific element.
		Player.element = $player = $(playerElement);

		// Get our control locations
		for (var g in controllocations) controllocations[g] = $player.$('.controls-' + g); // Get all our control locations

		$player.$('.controls')
			.on('click', function() {
				// Click displays the controls in case they're hidden
				player.toggleControls(true);
			});

		$player.$('.display')
			.on('dblclick', function() {
				// Toggle fullscreen on double click
				player.toggleFullscreen();
			});

		// Remove click propagation from all the inputs in controls
		// TODO: is this needed?
		$player.$('.controls button,input')
			.on('click', function(evt) {
				evt.stopPropagation();
			});

		$(document.body)
			.off('keydown')
			.on('keydown', function(evt) {
				Player.key(evt.keyCode);
			});

		console.log("Player attached to " + $player.e);
	}

	function config(config) {
		Player.config = config;

		// Set the document title
		if (config.title) document.title = config.title;

		// Build the controls for the media.
		buildControls(config.controls);

		// Configure loading elements.
		if (config.loading) {
			if (config.loading.text === false) {
				// Show the loading text?
				$player.$('.loading-text h2').css({ display: 'none' });
			}
			if (config.loading.bar === false) {
				// Show the loading bar?
				$player.$('.loading-text .loading-bar').css({ display: 'none' });
			}
			if (config.loading.image) {
				// Show a logo on top of the loading bar.
				$player.$('.loading .logo').set({ src: config.loading.image });
			}
		} else if (config.loading === false) {
			// Explicitly set to 'don't show loading at all'
			$player.$('.loading-text').css({ display: 'none' });
		}

		// Use background?
		if (config.bg) {
			$player.$$('.background .bg').set({ src: config.bg });
		}

		// Use blur?
		if (config.blur) {
			$player.$('.background .bg.blurred').css({
				transition: 'filter 1000ms'
			});
		}

		// Convert 'true' to 50% 50%
		if (config.cover === true) {
			config.cover = '50% 50%';
		}

		// Use cover instead of contain?
		if (config.cover) {
			$player.$$('.background .bg').css({ objectFit: 'cover', objectPosition: config.cover });
		} else {
			$player.$$('.background .bg').css({ objectFit: 'contain' });
		}
	}

	// Hide all loading elements
	function hideLoading() {
		$player.$$('.loading > div.full')
			.addClass('hidden')
	}

	// Show the progress bar with a loading text
	function showLoadingProgressBar(text) {
		console.log(text);
		hideLoading();

		$player.$('.loading-text')
			.removeClass('hidden')
			.$('h2')
			.text(text);
	}

	// Show the error element.
	function showError(text, message) {
		var error_el = $player.$('.error-message')
			.removeClass('hidden');

		error_el
			.$('h3')
			.text(message);

		error_el
			.$('h2')
			.text(text);
	}

	// Update the progress bar (0..1)
	function updateLoadingProgressBar(progress) {
		progress = (progress * 100).toFixed(2);
		$player.$('.loading-bar').css({ width: (progress) + '%' });
	}

	// Show the clicky button that the user needs to press before playback
	function showLoadingLoadButton(click) {
		$player.$('.loading-button')
			.removeClass('hidden')
			.$('button')
			.off('click')
			.one('click', click);
	}

	function buildControls(controls) {
		var defaults = {
			"pause": { "src": "control-pause", "location": "sw" },
			"volume": { "src": "control-volume", "location": "sw", "min": 0, "max": 1, "default": 0.5 },
			"speed": { "src": "control-speed", "location": "sw", "min": 0.5, "max": 3, "default": 1 },
			"hide": { "src": "control-hide", "location": "se" },
			"fullscreen": { "src": "control-fullscreen", "location": "se" }
		};

		// Explicitly set controls to false.
		if (controls === false) return;

		if (!controls) {
			for (var g in defaults) {
				attach_control(blueprint.build(defaults[g]), defaults[g].location);
			}
		} else {
			// Make sure we are dealing with an array every time
			controls = $.wrap(controls);

			for (var g = 0, gg = controls.length; g < gg; g++) {
				var control = controls[g];
				if (typeof control === "string") {
					// Convert strings like "pause" to their equivalent control.
					if (control in defaults) {
						control = defaults[control];
					} else throw new Error("Unknown control '"+control+"'!");
				}

				// Create a control using the blueprints
				attach_control(blueprint.build(control), control.location);
			}
		}
	}

	var player = {
		load: function (o) {
			o = o || {};
			o.config = o.config || {};

			// Attach to the right element
			attach(o.config.element || $('#player'));

			// Player-specific configuration
			config(o.config);

			// Go to engine
			try {
				Engine.load(o);
			}
			catch (err) {
				error(err);
			}
		},
		attach: function (e) { attach(e); },
		error: function (e) { error(e); },
		loading: function (name) {
			showLoadingProgressBar(name);
		},
		progress: function (progress) {
			updateLoadingProgressBar(progress);
		},
		// Show player, hide loading
		show: function (callback) {
			hideLoading();

			$player.$$('.background .bg')
				.removeClass('blurred')
				.css({ filter: 'none' });
			$player.$('.post').removeClass('hidden');

			callback();
			// TODO: Remove this timeout when we have a solid way of knowing when pre
			// has been substituted by the post fade in, and the blur has gone.
			setTimeout(function(){
				$player.$('.pre').addClass('hidden');
			}, 222);
		},
		// Request a click
		click: function (callback) {
			hideLoading();

			// Show the play button after a little timeout
			// Wrap the user input nicely in timeouts
			setTimeout(function() {
				showLoadingLoadButton(function() {
					hideLoading();
					setTimeout(callback, 500);
				});
			}, 500);
		},
		attachControls: function() {
			for (var g = 0, gg = arguments.length; g < gg; g++) {
				attach_control(arguments[g]);
			}
		},
		clearSceneControls: function() {
			$player.$$('button.scene').remove();
		},
		togglePaused: function() {
			Engine.setPaused(!Engine.paused);

			// Set all of us to play/paused
			if (Engine.paused) {
				$$('.control-pause .icon').set({ name: 'play' });
			} else {
				$$('.control-pause .icon').set({ name: 'pause' });
			}
		},
		toggleControls: function(force) {
			var e = $player.$('.controls');
			if (force == null) {
				e.toggleClass('hidden');
			} else {
				if (force) {
					e.removeClass('hidden');
				} else {
					e.addClass('hidden');
				}
			}

			console.log("Toggle Controls");
			$player.$$('.control-hide .icon').set({ name: e.hasClass('hidden') ? 'show' : 'hide' });
		},
		toggleFullscreen: function() {
			$player.fullscreen();

			$player.$$('.control-fullscreen .icon').set({ name: $player.isFullscreen() ? 'fullscreen' : 'exit-fullscreen' });
		},
		key: function(code) {
			console.log('Key ' + code);
			// Toggle pause by pressing space
			if (code === 32) {
				Player.togglePaused();
				return;
			}

			$$('button.scene[data-key-code="' + code + '"]').click();
		},
	};

	return player;
})();
