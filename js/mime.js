/**
 * Simple mime type handler.
 * */

var Mime = (function(){
	function Mime() {}

	var types = {
		'mp3': 'audio/mpeg',
		'wav': 'audio/wav',
		'ogg': 'audio/ogg',
		'mp4': 'video/mp4',
		'webm': 'video/webm',
	};

	$.extend(Mime, {
		getType: function(filename) {
			filename = filename.replace(/[?#].*$/, "");
			var ext = filename.split(".").pop();
			if (ext in types) {
				return types[ext];
			}
			return '';
		}
	});

	return Mime;
})();
