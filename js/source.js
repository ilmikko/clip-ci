/**
 * Handles the loading of sources, mostly preload and preplay.
 * */

var Source = (function() {
	function Source() {
		this.element = $('>div');
		this.init();
	}

	function urlcat(url, part) {
		if (!part) return url;

		// Which separator do we use in the url?
		var separator = '?';
		if (url.indexOf('?') > -1) separator = '&';
		return url + separator + part;
	}

	$.extend(Source.prototype, {
		init: function() {
			this.speed = this.baseSpeed = this.engineSpeed = this.faderSpeed = 1;
			this.volume = this.baseVolume = this.engineVolume = this.faderVolume = 1;
			this.opacity = this.baseOpacity = this.engineOpacity = this.faderOpacity = 1;
		},
		config: function(o) {
			o = o || {};

			this.fade = o.fade;

			if (o.speed != null) { this.baseSpeed = o.speed; this.updateSpeed(); }
			if (o.volume != null) { this.baseVolume = o.volume; this.updateVolume(); }
			if (o.opacity != null) { this.baseOpacity = o.opacity; this.updateOpacity(); }

			this.active = false;

			if (typeof Player !== 'undefined') {
				if (Player.config.cover) {
					this.element
						.css({ objectFit: 'cover', objectPosition: Player.config.cover })
						.$('img').css({ objectFit: 'cover', objectPosition: Player.config.cover });
				}
			}
		},

		progress: function() {
			this.active = false;
			if (this.next) {
				var next = this.next;
				this.next = null;
				next();
			}
		},

		preplayQuick: function(callback) {
			var self = this;
			self.element
				.pause()
				.prop({ currentTime: 0, playbackRate: self.speed, volume: self.volume })
				.off('timeupdate')
				.off('ended');

			self.prepareFade();

			if (callback) callback();
		},
		preplay: function(callback, update) {
			// Make sure we won't stutter on play start, so we play the media for a
			// couple of seconds beforehand.

			var bufferAmount = 3; // seconds

			var self = this;

			var once = false;
			function success() {
				if (once) return; else once = true;

				self.element
					.pause()
					.prop({ currentTime: 0, playbackRate: self.speed, volume: self.volume })
					.off('timeupdate')
					.off('ended');

				self.prepareFade();

				if (callback) callback();
			}

			this.element
				.prop({ playbackRate: 5, volume: 0 })
				.on('timeupdate', function() {
					var currentTime = this.e.currentTime;

					if (currentTime >= bufferAmount || currentTime >= this.e.duration) {
						success();
					} else {
						update(currentTime / bufferAmount);
					}
				})
				.one('ended', function() { success(); })
				.play();
		},
		prepareQuick: function(callback) {
			var self = this;
			self.element.prop({ playbackRate: self.speed });
			self.element.prop({ volume: self.volume });
			self.element.prop({ currentTime: 0 });

			// Loop through our sources, appending them to the element.
			for (var g = 0, gg = self.srcs.length; g < gg; g++) {
				var source = self.srcs[g];

				self.element
					.clear()
					.append(
						$('>source').set({
							src: source,
							type: Mime.getType(source)
						})
					);
			}

			self.element.load();

			if (callback) callback();
		},
		prepare: function(callback) {
			// Make sure this source is playable

			var self = this;
			var timeout = 10000; // milliseconds

			var once = false;
			function done() {
				if (once) return; else once = true;
				self.element.off("error");
				self.element.prop({ playbackRate: self.speed });
				self.element.prop({ volume: self.volume });
				self.element.prop({ currentTime: 0 });

				if (callback) callback();
			}

			// Automatic retry as some browsers hiccup on the loading sometimes (I know)
			(function retry(param) {
				// Wait a while until retrying
				var timeoutObject = setTimeout(function() {
					var readyState = self.element.e.readyState;

					if (readyState < 2) {
						console.warn(self.srcs + " is taking a while to load. (readyState=" + readyState + ")");
						self.element.off('error');
						retry('r=' + Math.random());
					} else {
						// Close enough. See audio.js for more information.
						console.warn(self.srcs + ' skipped preplay...');
						done();
					}
				}, timeout);

				self.element
					.on('error', function(err) {
						clearTimeout(timeoutObject);
						if (err) {
							console.error(err);
							if (err.message) {
								console.error(err.message);
							}
						}
						if (Player) Player.error("Media preparation error.\n" + JSON.stringify(err));
					})
					.one('canplaythrough', function() {
						clearTimeout(timeoutObject);
						done();
					})

				// Loop through our sources, appending them to the element.
				for (var g = 0, gg = self.srcs.length; g < gg; g++) {
					var source = self.srcs[g];

					self.element
						.clear()
						.append(
							$('>source').set({
								src: urlcat(source, param),
								type: Mime.getType(source)
							})
						);
				}

				self.element.load();
			})();
		},

		prepareFade: function() {
			if (!this.fade) return;
			if (typeof this.fade !== 'object') {
				this.fade = { in: this.fade, out: this.fade };
			}
			this.fader = new Fader(this, this.fade);
		},

		setSpeed: function(p) { this.baseSpeed = p; this.updateSpeed(); },
		setVolume: function(p) { this.baseVolume = p; this.updateVolume(); },
		setOpacity: function(p) { this.baseOpacity = p; this.updateOpacity(); },

		setEngineSpeed: function(p) { this.engineSpeed = p; this.updateSpeed(); },
		setEngineVolume: function(p) { this.engineVolume = p; this.updateVolume(); },
		setEngineOpacity: function(p) { this.engineOpacity = p; this.updateOpacity(); },

		setFaderSpeed: function(p) { this.faderSpeed = p; this.updateSpeed(); },
		setFaderVolume: function(p) { this.faderVolume = p; this.updateVolume(); },
		setFaderOpacity: function(p) { this.faderOpacity = p; this.updateOpacity(); },

		updateSpeed: function() { this.speed = this.baseSpeed * this.faderSpeed * this.engineSpeed; this.element.prop({ playbackRate: this.speed }); },
		updateVolume: function() { this.volume = this.baseVolume * this.faderVolume * this.engineVolume; this.element.prop({ volume: this.volume }); },
		updateOpacity: function() { this.opacity = this.baseOpacity * this.faderOpacity * this.engineOpacity; this.element.css({ opacity: this.opacity }); },

		show: function() {},
		hide: function() {},
	});

	return Source;
})();

$.extend(Source, {
	unloaded: [],
	prepared: [],
	sources: [],

	add: function(source) {
		this.sources.push(source);
		this.unloaded.push(source);
		return source;
	},

	audio: function(src) { return this.add(new SourceAudio($.wrap(src))); },
	video: function(src) { return this.add(new SourceVideo($.wrap(src))); },
	image: function(src) { return this.add(new SourceImage($.wrap(src))); },

	prepare: function(callback) {
		console.log("Preparing " + this.unloaded.length + " sources...");

		var prepareHandler = new LoadingHandler('PREPARING');
		var prepareCache = {};

		prepareHandler
			.on('finish', function() {
				console.log("Preparation complete!");
				callback();
			})
			.on('update', function(evt) { Player.progress(evt.progress); });

		// Wait for at least 200ms, and still finish if there are no sources
		prepareHandler.start('time');
		setTimeout(function() { prepareHandler.end('time'); }, 200);

		// Prepare unloaded sources
		var self = this;
		while (this.unloaded.length > 0) {
			var el = this.unloaded.shift();
			var id = 'prepare-' + el.srcs;

			var done = function(el) {
				Player.element.$('.display').append(el.element);
				self.prepared.push(el);
			}

			if (prepareCache[id]) {
				console.log("Skipping preparation of '" + id + "' (cached)");
				el.prepareQuick((function(el) {
					done(el);
				})(el));
				continue;
			}

			console.log("Preparing '" + id + "'...");
			prepareCache[id] = true;
			prepareHandler.queue((function(id, el) {
				return function() {
					prepareHandler.start(id);

					el.prepare(function () {
						prepareHandler.end(id);
						done(el);
					});
				}
			})(id, el));
		}

		prepareHandler.fire();
	},
	preplay: function(callback) {
		// Preplay ~5 seconds of each video (and audio) in order to mitigate stuttering when playing the real thing.
		console.log("Preplaying " + this.prepared.length + " sources...");

		var preplayHandler = new LoadingHandler('PREPLAYING');
		var preplayCache = {};

		preplayHandler
			.on('finish', function () {
				console.log("Preplaying complete!");
				setTimeout(function(){
					callback();
				}, 500);
			})
			.on('update', function (evt) { Player.progress(evt.progress); });

		// Wait for at least 2000ms, and still finish if there are no sources
		var delay = 2000;
		preplayHandler.start('time');
		setTimeout(function() { preplayHandler.end('time'); }, delay);

		while (this.prepared.length > 0) {
			var el = this.prepared.shift();
			var id = 'preplay-' + el.srcs;

			var done = function(id) {}

			if (preplayCache[id]) {
				console.log("Skipping preplay of '" + id + "' (cached)");
				el.preplayQuick((function(id) {
					done(id);
				})(id));
				continue;
			}

			console.log("Preplaying '" + id + "'...");
			preplayCache[id] = true;
			preplayHandler.start(id);
			setTimeout((function(id, el) {
				return function() {
					el.preplay(function () {
						preplayHandler.end(id);
						done(id);
					}, function(progress) {
						preplayHandler.update(id, progress);
					});
				}
			})(id, el), this.prepared.length*100);
		}

		// This is for artificially updating the progress bar.
		for (var g = 0, gg = 7; g < gg; g++) {
			var p = g/gg;
			setTimeout((function(p) {
				return function() {
					preplayHandler.update('time', p);
				}
			})(p), delay*p);
		}

		preplayHandler.fire();
	},
	hotplay: function(callback) {
		console.log("Hotplay...");
		// Try if we can skip the user input step (desktop UX)
		// Some browsers prevent autoplaying videos, this is for them.

		function skip() {
			callback();
		}

		function noskip() {
			// Request a click from the player (the "load" button), then continue
			Player.click(function() {
				callback();
			});
		}

		// We should be able to autoplay video elements fine.
		// However, if we have any audio elements, we need to ask the user to click a button first.

		var videos = [], audios = [];

		var sources = this.sources;
		for (var g = 0, gg = sources.length; g < gg; g++) {
			var el = sources[g];
			if (el instanceof SourceAudio) { audios.push(el); break; }
			if (el instanceof SourceVideo) { videos.push(el); }
		}

		// TODO: We could also start playing the scene muted, but this would mean there will be sync issues with the audio.
		// We would therefore have to create a progress tracker for audio elements that cannot be played.
		if (audios.length > 0) {
			console.warn("Not hotplaying as there are audio elements in the scene.");
			return noskip();
		}

		if (videos.length == 0) {
			// There doesn't seem to be any media files that need this functionality. We can skip!
			console.log('Hotplaying because there are no media files that need clicks.');
			return skip();
		}

		var p;
		var g = videos[0].element;
		try{ p = g.e.play(); }
		catch(err) {
			// failure
			console.warn(err + ' - Error. Not hotplaying.');
			g.e.pause();
			return noskip();
		}
		if (p) {
			// p is a promise
			p.then(function () {
				// success, continue fast route
				console.warn('Hotplay successful');
				g.e.pause();
				skip();
				// noskip();
			}, function (err) {
				// failure
				console.warn(err + ' but its okay. Continuing the longer route.');
				g.e.pause();
				noskip();
			});
		} else {
			// In IE, p is not a promise
			// Oh you, IE!
			g.e.pause();
			noskip();
		}
	},
});
