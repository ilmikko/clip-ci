function Media(o) {
	o = o || {};

	this.id = o.id;
	this.children = [];
}

Media.prototype = {
	assign: function(sceneid) {
		this.scene = sceneid;
		if (!this.children) return;
		for (var g = 0, gg = this.children.length; g < gg; g++) { this.children[g].assign(sceneid); }
	},

	getSources: function() {
		if (this.sources) return this.sources;
		var sources = [];
		if (this.children) for (var g = 0, gg = this.children.length; g < gg; g++) { Array.prototype.push.apply(sources, this.children[g].getSources()); }
		return this.sources = sources;
	},

	eachChild: function(proc) {
		if (!this.children) return;
		for (var g = 0, gg = this.children.length; g < gg; g++) proc(this.children[g]);
	},
	eachSource: function(proc) {
		if (!this.sources) this.getSources();
		for (var g = 0, gg = this.sources.length; g < gg; g++) proc(this.sources[g]);
	},

	setEngineSpeed: function(speed) { this.eachSource(function(source) { source.setEngineSpeed(speed); }); },
	setEngineVolume: function(volume) { this.eachSource(function(source) { source.setEngineVolume(volume); }); },
	setEngineOpacity: function(opacity) { this.eachSource(function(source) { source.setEngineOpacity(opacity); }); },

	pause: function() { this.eachSource(function(source) { source.pause(); }); },
	resume: function() { this.eachSource(function(source) { source.resume(); }); },

	play: function() { this.eachSource(function(source) { source.play(); }); },
	stop: function() { this.eachSource(function(source) { source.stop(); }); this.progress(); },

	show: function() {},
	hide: function() { this.eachChild(function(child) { child.hide(); }); },

	progress: function() {
		if (this.next) {
			var next = this.next;
			this.next = null;
			next();
		}
	},
};

$.extend(Media, {
	create: function(o) {
		o = o || {};

		if (o.id && media.ids[o.id]) {
			// If we have created this element already, return it instead.
			return Media.ids[o.id];
		}

		var elements = {
			queue: Queue,
			loop: Loop,
			random: Random,
			goto: Goto,
			video: Video,
			audio: Audio,
			image: Image,
			button: Button,
			delay: Delay,
			all: All,
			sync: Sync,
			stop: Action,
			play: Action
		};

		if (!o.src) throw new Error("No `src` property in media element `" + JSON.stringify(o) + "`");
		if (!(o.type in elements)) throw new Error("Invalid type `" + o.type + "`");

		var el = new elements[o.type](o);

		if (o.id) { Media.ids[o.id] = el; }

		return el;
	},
	expand: function expand(o) {
		// Leave leaves alone (footnote: English is a peculiar language)
		if (typeof o !== 'object') return o;

		if (o.length && !isNaN(o.length)) {
			for (var g = 0, gg = o.length; g < gg; g++) {
				o[g] = expand(o[g]);
			}
			return o;
		}

		if (!o.type) {
			// In some cases, it is possible to determine the media type from the keys.
			var matches = [];

			var elements = {
				queue: 1,
				loop: 1,
				random: 1,
				goto: 1,
				video: 1,
				audio: 1,
				image: 1,
				button: 1,
				delay: 1,
				all: 1,
				sync: 1,
				play: 1,
				stop: 1,
			};

			for (var g in elements) {
				if (g in o) matches.push(g);
			}

			if (matches.length != 1) {
				console.error(o);
				throw new Error("Cannot determine type of media source!");
			}

			o.type = matches[0];
			o.src = o[o.type];
			delete o[o.type];
		}

		// Aliases.
		if (o.wait && o.type != 'all') {
			// { ..., "until": { ... } } --> { "all": [ .... ], "until": { ... } }
			var until = o.wait;
			delete o.wait;
			o = { 'type': 'all', 'src': o, 'until': until };
		}

		if (o.next) {
			// { ..., "next": { ... } } --> { "queue": [ ...., { ... } ] }
			var next = expand(o.next);
			delete o.next;

			o = { 'type': 'queue', 'src': [ o ] };
			// Combine queues into one large queue.
			if (o.type === 'queue' && next.type === 'queue') {
				Array.prototype.push.apply(o.src, next.src);
			} else {
				o.src.push(next);
			}
		}

		// Recurse to leaves
		if (o.until) o.until = expand(o.until);
		if (o.wait) o.wait = expand(o.wait);
		if (o.with) o.with = expand(o.with);
		if (o.src) o.src = expand(o.src);

		return o;
	}
});
