function verticalSlider(o) {
	o = o || {};

	var thumb = $('>div').addClass('thumb'), track = $('>div').addClass('track');

	var self = $('>div').addClass('vertical-slider');

	function updateSlider(p) {
		thumb.css({ bottom: (p * 100).toFixed(1) + "%" });
	}

	var value = 0.5;
	updateSlider(value);

	function drag(evt) {
		// Get track
		var rect = track.e.getBoundingClientRect();

		// Calculate percentage
		var clientY = evt.clientY;

		// Touch support
		if (evt.touches && evt.touches.length) clientY = evt.touches[0].clientY;

		value = 1 - (clientY - rect.top) / rect.height;

		// Snap to middle (0.5)
		var epsilon = 0.05;
		if (Math.abs(value - 0.5) < epsilon) value = 0.5;

		// Clamp to value
		else if (value < 0) value = 0;
		else if (value > 1) value = 1;

		updateSlider(value);

		self.fire('input');
	};

	function down(evt) {
		dragging = true;

		var body = $(document.body);

		// Initial click is also registered
		drag(evt);

		function up(evt) {
			// Disable mouse capture
			body
				.off('mouseup', up)
				.off('mousemove', drag)
				.off('touchend', up)
				.off('touchmove', drag);

			self.fire('change');
		}

		// Enable mouse capture
		body
			.one('mouseup', up)
			.on('mousemove', drag)
			.one('touchend', up)
			.on('touchmove', drag);
	}

	// Not enough people appreciate this
	self.on('contextmenu', function (evt) {
		evt.preventDefault();
		updateSlider(value = 0.5);
		self.fire('input');
		return false;
	});

	self.on('touchstart', down);
	self.on('mousedown', down);

	var max = o.max || 1;
	var mid = o.mid || 0.5;
	var min = o.min || 0;

	self.extend({
		value: function () {
			// Convert 0..1 to whatever value was specified
			// h(x)=-4 (mid- (max+min)/(2))x^(2)+ (max-min+4 (mid- (max+min)/(2)))x+min

			// Quick maths to get the quadratic value based on min,mid and max
			var b = mid - (max + min) / 2;
			return -4 * b * value * value + (max - min + 4 * b) * value + min;
		},
		dragTo: function (_value) {
			if (_value > 1) _value = 1;
			else if (_value < 0) _value = 0;

			value = _value;

			updateSlider(value);

			self.fire('input');
			self.fire('change');
		}
	});

	return self.append(track, thumb)
}
