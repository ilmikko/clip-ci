require('./test/lib.js');

load('js/alt.js');
load('js/mime.js');

// .ogg can also be a video, however we assume it is an audio here.

function test_simple_audio_mimes() {
	assert.strictEqual(Mime.getType("Song.mp3"), "audio/mpeg");
	assert.strictEqual(Mime.getType("Audio.wav"), "audio/wav");
	assert.strictEqual(Mime.getType("Something.else.ogg"), "audio/ogg");
}

function test_simple_video_mimes() {
	assert.strictEqual(Mime.getType("LookAtThisNet.mp4"), "video/mp4");
	assert.strictEqual(Mime.getType("MusicVideo.webm"), "video/webm");
}

function test_source_urls() {
	assert.strictEqual(Mime.getType("/some/url/Song.mp3"), "audio/mpeg");
	assert.strictEqual(Mime.getType("/some/url/LookAtThisNet.mp4?query=test"), "video/mp4");
	assert.strictEqual(Mime.getType("/some/url/MusicVideo.webm#fragment"), "video/webm");
}
