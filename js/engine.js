// An improved clipci engine which can handle recursive scenes

var Engine = (function() {
	var scenes = {};
	var playing = {};

	function load(json) {
		$.extend(scenes, json.scenes);

		// ID 'main' is always the main media ID.
		// TODO: Make sure there are no collisions here.
		if (json.main && !scenes.main) {
			scenes.main = json.main;
		}

		for (var id in scenes) {
			scenes[id] = Media.expand(scenes[id]);
			scenes[id] = Media.create(scenes[id]);
			scenes[id].assign(id);
		}

		Player.loading("Preparing...");
		Source.prepare(function(){
			Source.hotplay(function(){
				Player.loading("Loading...");
				Source.preplay(function(){
					// Show the media
					console.log("Showing media...");
					Player.show(function(){
						play('main');
					});
				});
			});
		});
	}

	function setSpeed(speed, cheap) {
		Engine.speed = speed;

		Fader.setSpeed(speed);

		console.log("Setting speed to " + speed);
		for (var g in playing) {
			if (playing[g].setEngineSpeed) playing[g].setEngineSpeed(speed);
		}
	}

	function setVolume(volume, cheap) {
		Engine.volume = volume;

		console.log("Setting volume to " + volume);
		for (var g in playing) {
			if (playing[g].setEngineVolume) playing[g].setEngineVolume(volume);
		}
	}

	function setPaused(paused) {
		Engine.paused = !!paused;
		if (Engine.paused) {
			// Player set to paused, pause all our sources!
			console.log("Pausing all scenes...");
			Timeout.pause();
			Fader.pause();
			for (var g in playing) playing[g].pause();
		} else {
			console.log("Resuming all scenes...");
			Timeout.resume();
			Fader.resume();
			for (var g in playing) playing[g].resume();
		}
	}

	function play(scene, callback) {
		var el = getScene(scene);
		console.log("Playing:", el);
		playing[scene] = el;

		el.next = callback;
		el.play();
	}

	function stop(scene, callback) {
		var el = getScene(scene);
		console.log("Stopping:", el);
		delete playing[scene];

		el.next = callback;
		el.stop();
	}

	function hide(scene) {
		var el = getScene(scene);
		console.log("Hiding:", el);

		el.hide();
	}

	function stop_all() {
		var stopHandler = new LoadingHandler('ENGINE_STOP');

		stopHandler.on('finish', function() { console.log("Stophandler finished"); callback(); });

		for (var id in playing) {
			console.log("Stopping:", id);
			stopHandler.start(id);
			var scene = getScene(id);

			scene.next = function() {
				stopHandler.end(id);
				delete playing[id];
			}
			scene.stop();
		}

		stopHandler.fire();
	}

	function getScene(id){
		if (!(id in scenes)) {
			Player.error("Cannot find scene by id '" + id + "'");
			throw new Error("Cannot find scene by id: '" + id + "'!");
		}
		return scenes[id];
	}


	var Engine = {
		paused: false,
		volume: 0.5,
		speed: 1,
		opacity: 1,
		load: load,
		// TODO: These could be somewhere else
		setSpeed: setSpeed,
		setVolume: setVolume,
		setPaused: setPaused,
		// TODO: These are only here to work with the scene functions, but
		// really the scene functions should change and not the engine publics.
		play: play,
		stop: stop,
		hide: hide,
	};

	return Engine;
})();
