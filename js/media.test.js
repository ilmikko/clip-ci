require('./test/lib.js');

load('js/alt.js');
load('js/media.js');

function test_expand_type() {
	assert.deepStrictEqual(Media.expand({
		"type": "video",
		"src": "video.mp4"
	}), {
		"type": "video",
		"src": "video.mp4"
	});
	assert.deepStrictEqual(Media.expand({
		"video": "video.mp4"
	}), {
		"type": "video",
		"src": "video.mp4"
	});
}

// TODO: Test ambiguities

function test_expand_wait() {
	assert.deepStrictEqual(Media.expand({
		"loop": {
			"video": "video.mp4"
		},
		"wait": {
			"delay": 2000
		}
	}), {
		"type": "all",
		"src": {
			"type": "loop",
			"src": {
				"type": "video",
				"src": "video.mp4"
			}
		},
		"until": {
			"type": "delay",
			"src": 2000
		}
	});
}

function test_expand_next() {
	assert.deepStrictEqual(Media.expand({
		"video": "video.mp4",
		"next": {
			"video": "video2.mp4"
		}
	}), {
		"type": "queue",
		"src": [
			{
				"type": "video",
				"src": "video.mp4"
			},
			{
				"type": "video",
				"src": "video2.mp4"
			}
		]
	});

}

function test_expand_next_merge_queue() {
	assert.deepStrictEqual(Media.expand({
		"video": "video.mp4",
		"next": {
			"video": "video2.mp4",
			"next": {
				"video": "video3.mp4",
				"next": {
					"video": "video4.mp4"
				}
			}
		}
	}), {
		"type": "queue",
		"src": [
			{
				"type": "video",
				"src": "video.mp4"
			},
			{
				"type": "video",
				"src": "video2.mp4"
			},
			{
				"type": "video",
				"src": "video3.mp4"
			},
			{
				"type": "video",
				"src": "video4.mp4"
			}
		]
	});
}
