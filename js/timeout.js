/**
 * Controls the time delay properties for media elements.
 * */

var Timeout = (function() {
	var id = 0;
	var timeouts = {};

	function Timeout(callback, delay) {
		var self = this;

		this.callback = function() {
			self.stop();
			callback();
		};

		this.speed = 1;
		this.delay = delay;
		this.id = id++;

		this.active = false;

		this.play();
		this.setSpeed(Engine.speed);
	}

	Timeout.prototype = {
		play: function() {
			this.active = true;
			timeouts[this.id] = this;

			this.resume(Date.now() / 1000);
		},
		stop: function() {
			this.active = false;
			delete timeouts[this.id];

			this.pause(Date.now() / 1000);
		},
		pause: function(now) {
			if (!this.active) return;
			if (this.timeout) {
				clearTimeout(this.timeout);
				this.timeout = null;
				this.delay -= (now - this.start);
			}
		},
		resume: function(now) {
			if (!this.active) return;
			if (!this.timeout) {
				var delay = this.delay * 1000;
				this.timeout = setTimeout(this.callback, delay);
				this.start = now;
			}
		},
		setSpeed: function(speed) {
			if (!this.active) return;
			if (this.timeout) {
				var now = Date.now() / 1000;
				this.pause(now);
				this.setSpeed(speed);
				this.resume(now);
			}

			this.delay /= speed / this.speed;
			this.speed = speed;
		}
	};

	$.extend(Timeout, {
		pause: function() {
			var now = Date.now() / 1000;
			for (var timeout in timeouts) timeouts[timeout].pause(now);
		},
		resume: function() {
			var now = Date.now() / 1000;
			for (var timeout in timeouts) timeouts[timeout].resume(now);
		}
	});

	return Timeout;
})();
