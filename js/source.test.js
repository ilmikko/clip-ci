require('./test/lib.js');

load('js/alt.js');
load('js/source.js');

function test_speed_settings() {
	var source = new Source();

	assert.strictEqual(source.speed, 1);

	source.setSpeed(2);
	assert.strictEqual(source.speed, 2);

	source.setSpeed(1);
	assert.strictEqual(source.speed, 1);

	source.setEngineSpeed(0.5);
	assert.strictEqual(source.speed, 0.5);

	source.setSpeed(2);
	assert.strictEqual(source.speed, 1);

	source.setEngineSpeed(1);
	assert.strictEqual(source.speed, 2);

	source.setFaderSpeed(0.5);
	source.setEngineSpeed(0.5);
	assert.strictEqual(source.speed, 0.5);

	source.setFaderSpeed(0);
	assert.strictEqual(source.speed, 0);

	source.setFaderSpeed(1);
	assert.strictEqual(source.speed, 1);
}

function test_volume_settings() {
	var source = new Source();

	assert.strictEqual(source.volume, 1);

	source.setVolume(2);
	assert.strictEqual(source.volume, 2);

	source.setVolume(1);
	assert.strictEqual(source.volume, 1);

	source.setEngineVolume(0.5);
	assert.strictEqual(source.volume, 0.5);

	source.setVolume(2);
	assert.strictEqual(source.volume, 1);

	source.setEngineVolume(1);
	assert.strictEqual(source.volume, 2);

	source.setFaderVolume(0.5);
	source.setEngineVolume(0.5);
	assert.strictEqual(source.volume, 0.5);

	source.setFaderVolume(0);
	assert.strictEqual(source.volume, 0);

	source.setFaderVolume(1);
	assert.strictEqual(source.volume, 1);
}

function test_opacity_settings() {
	var source = new Source();

	assert.strictEqual(source.opacity, 1);

	source.setOpacity(2);
	assert.strictEqual(source.opacity, 2);

	source.setOpacity(1);
	assert.strictEqual(source.opacity, 1);

	source.setEngineOpacity(0.5);
	assert.strictEqual(source.opacity, 0.5);

	source.setOpacity(2);
	assert.strictEqual(source.opacity, 1);

	source.setEngineOpacity(1);
	assert.strictEqual(source.opacity, 2);

	source.setFaderOpacity(0.5);
	source.setEngineOpacity(0.5);
	assert.strictEqual(source.opacity, 0.5);

	source.setFaderOpacity(0);
	assert.strictEqual(source.opacity, 0);

	source.setFaderOpacity(1);
	assert.strictEqual(source.opacity, 1);
}
